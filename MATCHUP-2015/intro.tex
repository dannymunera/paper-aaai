\section{Introduction}
\label{sec:introduction}

In 1962, Gale and Shapley introduced the Stable Matching (SM)
problem~\cite{Gale1962}.  An SM instance of size $n$ involves a set of
$n$ men and a set of $n$ women, each of whom has ranked all members
of the other set in strict order of preference.  Solving such a
problem consists of finding a \emph{matching}, i.e.~a one-to-one
matching between the men and the women.  In addition the matching must
be \textit{stable}, meaning that there is no man-woman pair where both
would rather marry each other than their current partner -- such a
pair is called a \textit{blocking pair}.  Gale and Shapley proved that
such a stable matching always exists and proposed an $O(n^2)$ algorithm
(called GS in what follows) to find one.

However, requiring \textit{each} member to rank \textit{all} members
of the opposite sex in a \textit{strict} order is too strong a
restriction for many real-life, large-scale applications.  A natural
variant of SM is the \emph{Stable Matching with Ties and Incomplete Lists
} (SMTI) problem~\cite{Iwama2008,Manlove2002}.  In SMTI, the
preference lists may include ties (to express indifference) and may be
incomplete (to express that some partners are unacceptable). More
formally, an SMTI instance of size $n$ consists of $n$ men and $n$
women, and a preference list for each of them, which contains some of
the people of the other gender.
% \footnote{While not required by the
%   theory we assume that the preference list for woman $w$ does not
%   contain man $m$ iff the list for $m$ does not contain $w$.  This is
%   trivial to establish with a pre-pass.}.
Such preference lists are weak orders, that is, total orders possibly
containing ties.  Given an SMTI instance, a \emph{matching} $M$ is a set of
pairs $(m,w)$ representing a (possibly partial) one-to-one matching of men
and women.  If a man $m$ is not matched in $M$ (i.e.{} for no $w$ is it the
case that $(m,w)\in M$), we say that $m$ is \emph{single} in $M$ (similarly
for women).  The \emph{size} of a matching $M$ is the cardinality of $M$,
denoted $|M|$.


With the introduction of ties in the preference lists, three different
notions of stability may be
used~\cite{Irving1994,Manlove2002,Iwama2008}.  As we consider only
\emph{weak stability} (the most challenging), we simply call it
\emph{stability}.  In the context of $M$, a pair $(m,w)$ is a
\emph{Blocking Pair} (BP) iff (a)~$m$ and $w$ accept each other and
(b)~$m$ is either single in $M$ or strictly prefers $w$ to his current
wife, and (c)~$w$ is either single in $M$ or strictly prefers $m$ to
her current husband.  A matching $M$ is stable iff it has no blocking
pairs.

A (weakly) stable matching always exists and can be found with
variants of the GS algorithm.  Since any given SMTI instance may have
stable matchings of different sizes, a natural requirement is to find
one of maximum cardinality.  This optimization problem (called
MAX-SMTI) has many real-life applications~\cite{Manlove2002,Iwama2008}
and has attracted a lot of research in recent years because of that:
car sharing or bipartite market sharing, job markets and social
networks.  Many of these applications involve very large sets.
Unfortunately, the MAX-SMTI problem has been shown to be NP-hard, even
for very restricted cases (e.g. only men declare ties, ties are of
length two, the whole list is a tie)~\cite{Iwama1999,Manlove2002}.

We have recently proposed a Local Search (LS) algorithm for the SMTI
problem~\cite{Munera2015}.  For this, an SMTI problem is first modeled as a
permutation problem and then solved by the \emph{Adaptive Search} (AS)
method~\cite{Codognet2001,Codognet2003}.  Basically, starting from a random
matching, our algorithm iteratively tries to improve the current matching by
performing a swap between two variables (i.e. two men exchange their
partner).  For this, a limited neighbourhood is explored and the most
promising swap is selected based on a heuristic which selects the most
significant blocking pair to fix and/or a single man to marry.  The algorithm
stops when a \emph{perfect matching} is found (a stable matching with no singles) or
when a given timeout is reached (in which case the best matching found so far
is returned).  This algorithm turned out to have very high performance and is
able to optimally solve several large instances.


Another very useful variant of SM is the \textit{Hospitals / Residents
  problem with Ties}
(HRT)~\cite{Irving2000,Irving2009,Manlove2002}. An HRT instance
consists of two sets: the residents $R=\{r_1, \ldots r_{n_1}\}$ who
apply to the hospitals $H=\{h_1, \ldots h_{n_2}\}$.  The preference
list of a resident $r_i \in R$ consists of the ordered list of
\emph{acceptable} hospitals (a subset of $H$).  The preference list of
a hospital $h_j \in H$ contains the ordered list of residents (a
subset of $R$) who consider $h_j$ acceptable.  All preference lists
are allowed to contain ties.  In addition, each hospital $h_j \in H$
has a capacity $c_j$ indicating the maximum number of positions it
offers. 

% Figure~\ref{fig:hrt-ins} presents an example of HRT problem
% where brackets in the preference lists are used to represent ties.
% \begin{figure}[H]
%   \centering
%   \begin{tabular}{llllllllll}
%     \multicolumn{3}{l}{residents' preference lists}& $\qquad$ 
%     & \multicolumn{6}{l}{hospital's preference lists}\\
%     $r_1:$ &$h_2$& $h_1$     & & $h_1: (1) :$&$r_3$& $(r_4$&$r_5$& $r_1$& $r_2)$\\
%     $r_2:$ &$h_1$& $h_3$     & & $h_2: (3) :$&$(r_3$& $r_5$&$r_1)$& $r_6$& $r_4$\\
%     $r_3:$ &$h_1$& $h_2$     & & $h_3: (2) :$&$r_6$& $r_2$ & & & \\
%     $r_4:$ &$h_2$& $h_1$     & & & & && & \\
%     $r_5:$ &$h_2$& $h_1$     & & & & && & \\
%     $r_6:$ &$h_3$& $h_2$     & & & & && & \\    
%   \end{tabular}  
%   \caption{Example of a HRT problem instance}
%   \label{fig:hrt-ins}
% \end{figure}
The problem consists of finding a stable matching between residents
and hospitals satisfying both the preference lists (the matching must
be stable) and the capacities (each resident being assigned to at most
one hospital and the number of residents assigned to any hospital
$h_j$ must not exceed $c_j$).  At any stage during the matching
process, a hospital $h_j$ with $a_j$ assignees is said to be
\textit{over-subscribed} if $a_j > c_j$, \textit{full} if $a_j = c_j$,
and \textit{under-subscribed} if $a_j < c_j$.

The previously discussed notion of weak
stability can be adapted to HRT: in the context of $M$, a pair $(r,h)$ 
give rise to a blocking
pair iff (a) $r$ and $h$ accept each other and (b) $r$ is either
unassigned in $M$ or strictly prefers $h$ to his assigned hospital,
and (c) $h$ is either under-subscribed
 or strictly prefers $r$ to the worst
resident assigned to it.  As for SMTI, a matching $M$ is stable iff it
has no blocking pairs.

HRT problem has many practical applications, e.g.~assignment of
applicants to positions in job markets.  In the medical employment
domain, there are national programs in various countries such as the
Scottish Foundation Allocation Scheme (SFAS), the Canadian Resident
Matching Service (CARMS) or the National Resident Matching Program
(NRMP) in the USA.  Obviously, such programs involve very large sets.
Unfortunately, as for SMTI, the problem of finding a stable matching
of maximum cardinality (called MAX-HRT) for a given instance of HRT is NP-hard (even
for restricted cases, e.g.~if the ties are only allowed on one side).
Finding an efficient algorithm to solve HRT problems is thus a true
challenge with many real applications.

We deem it interesting to see if we can attack the HRT problem with our LS
algorithm.  While SMTI is a special case of HRT (where each hospital has
capacity one) we chose to adopt a reverse approach, considering an HRT as a
special case of SMTI so as to keep the main lines of our algorithm
(permutation-based, which ensures a compact memory representation and an
implicit modeling of the \emph{all-different} constraint).  To this end, we
can use the so-called \emph{cloning technique}~\cite{Gusfield1989} which
basically consists of creating $c_j$ copies of the hospital $h_j$, 
each of capacity $1$, and to use
these copies (inside a tie) each time this hospital is referenced in a
resident's preference list.  Strictly speaking, the resulting problem
instance is not exactly an STMI instance since the resulting sets (residents
and cloned hospitals) can have different sizes but it is trivial to add dummy
elements (residents or hospitals).  The extension of our algorithm to deal
with this feature is very simple.  All of this makes HRT and SMTI equivalent
problems.

This \emph{RISC}-like approach is analogous to what occurs with SAT
modeling: the object formulation is often voluminous and cumbersome
but its resolution by the best SAT solvers is very efficient -- often
faster than what is obtained with dedicated solvers which take
higher-level formulations, such as CSP or constraint programming.
Upon dealing with hard problem instances, we try to improve the solver
at low level, meaning that the techniques which we may come up with to
better solve HRT will also benefit SMTI, in the general case.  We
stress that our LS algorithm gets much better performance than
complete methods (i.e. enumerative, branch and bound, linear and integer programming, etc.),
% What is meant by complete methods?
even though we do not always reach the optimum
solution.
% Similarly, this paper describes our initial experimental findings,
% leaving much room for improvement, which we are already exploring.

As the rest of this paper will show, we manage to get very competitive
performance on real-world data sets of considerable size and
difficulty.  We are also convinced that this will further benefit from
the efficiency improvements we explored in~\cite{Munera2015}, namely
parallelism.

% The rest of this paper is organized as follows: section~\ref{sec:as-smti}
% presents our base LS algorithm and its performance
% evaluation. Section~\ref{sec:as-hrt}. A short conclusion together with ideas
% for future work ends the paper.


% \subsection*{SMTI and HRT algorithms}

% Finding efficient algorithms to solve SMTI has been an active field of
% research for several years, driven by its applications.  SMTI has been
% shown to be APX-hard~\cite{Halldorsson2003} and most of the recent
% research focuses on designing efficient \emph{approximation
%   algorithms}, i.e.~algorithms running in polynomial time yet able to
% guarantee solutions within a constant factor of the
% optimum~\cite{Kiraly2011}.  The current best are 3/2-approximation
% algorithms~\cite{Mcdermid2009,Kiraly2013,Paluch2014}.  An
% $r$-approximation algorithm always finds a stable matching $M$ with
% $|M| \geq |M_{opt}| / r$ where $M_{opt}$ is a stable matching of
% maximum size.  SMTI cannot be approximated beyond a factor 21/19 and
% probably not 4/3 either~\cite{Halldorsson2007}.  Approximation
% algorithms only find one solution for a given problem, while it is
% often useful to provide multiple (quasi-)optimal solutions.

% Constraint Programming (CP) can be used to solve SMTIs.  While there
% are some efforts to solve SM problems~\cite{Gent2001}, including the
% introduction of new global constraints to achieve efficient
% consistency~\cite{Unsworth2013,Manlove2007}, few papers are devoted to
% the MAX-SMTI variant.  The reference paper~\cite{Gent2002} is more a
% study of the properties of SMTI problems of limited size
% (i.e.~$n \leq 60$) than a search for efficient encodings to solve SMTI
% using CP.

% Surprisingly, SAT solvers have not been extensively used for SMTI.
% The work by Gent~\cite{Gent2002a} proposes a SAT encoding for SMTI and
% uses the Chaff solver for its evaluation.
% Linear~\cite{Roth93,VandeVate89} and integer
% programming~\cite{Kwanashie2013} have been also studied for variants
% of SM like the Hospitals/Residents problem (HR).  Recently, Local
% Search has been successfully applied to SMTI, for instance
% in~\cite{Gelain2013}, the authors propose LTIU, a local search
% algorithm with very good performance.

% Local Variables:
% mode: tex
% mode: reftex
% TeX-master: "MATCHUP15.tex"
% End:
