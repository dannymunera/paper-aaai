\section{Parallelization}
\label{sec:parallel-eval}

Parallel versions of local search procedures have been proposed
already~\cite{Alba2005,Alba2013}.  In this article we are interested in
\emph{multi-walks} methods (also called \emph{multi-starts}) which consist in
a concurrent exploration of the search space, either \emph{independently} or
\emph{cooperatively} with some communication between concurrent
processes. The \emph{Independent Multi-Walks} method
(IW)~\cite{Verhoeven1995} is the easiest to implement since the solver
instances do not communicate with each other. However, the resulting gain 
%is not optimal in most cases and 
tends to flatten when scaling over a hundred of
processors~\cite{CaniouCRDA:2014:Constraints}, and can be improved upon. 
In the \emph{Cooperative
  Multi-Walks} (CW) method~\cite{DBLP:dblp_journals/pc/ToulouseCS04}, the
solver instances exchange information (through communication), hoping to
hasten the search process. However, implementing an efficient cooperative
method is a very complex task: several choices have to be made about the
communication~\cite{DBLP:dblp_journals/pc/ToulouseCS04} which influence each
other and which are problem-dependent.

\subsection{The Cooperative Parallel Local Search Framework}
We build on the framework for Cooperative Parallel Local Search
({\coop}) proposed in~\cite{Munera2014a}. This framework, made
available as an open source library in X10, allows the
programmer to tune the search process through an extensive set of
parameters. 
% One of the key features is the ability to specify the
%trade-off between intensification and diversification in the search.
{\coop} augments the IW strategy with a tunable communication
mechanism, which allows for the cooperation between the multiple
instances to seek either an intensification or diversification
strategy for the search.

\textit{Explorer nodes} are the basic components in the framework:
each consists in a local search solver instance.  The point is to use
all the available processing units by mapping each \textit{explorer
  node} to a physical core.  Explorer nodes are grouped into
\textit{teams}, of fixed parametric size.
% The size of a teams is derived from the number of used cores and the number
% of teams.
%%
Each team seeks to \emph{intensify} the search in the most promising neighborhood
found by any of its members.  The parameters which guide the intensification
are the \textit{Report Interval} ($R$) and \textit{Update Interval} ($U$):
every $R$ iterations, each explorer node sends its current configuration and
the associated cost to its \textit{head node}.  The head node is the team
member which collects and processes this information, retaining the best
configurations in an \textit{Elite Pool} ($EP$) whose size is $|EP|$.  Every
$U$ iterations, explorer nodes randomly retrieve a configuration from the
$EP$, in the head node.  An explorer node may \emph{adopt} the configuration
from the $EP$, if it is ``better'' than its own current configuration with a
probability \textit{pAdopt}.  Simultaneously, the teams implement a mechanism
to cooperatively \emph{diversify} the search, i.e.~they try to extend the search to
different regions of the search space.  A detailed description of this
framework may be found in~\cite{Munera2014a}.
                                % (see Figure~\ref{fig-coop-description}).

%% spa: plus générique: peut gérer des solveurs hybrides (Tabou/GA/...)

%The {\coop} framework is generic, as it may accomodate hybrid solvers, in a
%portfolio-like manner~\cite{DBLP:journals/ai/GomesS01} including Tabu search,
%Genetic Algorithms and other similar approaches.

%% spa: on utilise une implem X10

%One notable aspect of the {\coop} framework is the availability of an
%open-source implementation, in X10. 
%This enables
%experimentation on different parallel architectures, while retaining a
%high-level formulation.

% \begin{figure}[htb]
% \centerline{\includegraphics[width=1.1\columnwidth]{images/TeamStrategy5}} 
% \caption{Cooperative Framework Description}
% \label{fig-coop-description}
% \end{figure}

\subsection{Independence vs Cooperation}

In this section we assess the parallelization of the AS model for SMTI
testing both IW and CW strategies. These experiments are made relatively
simple when using the X10 implementation of the {\coop} framework which
clearly separates the local search algorithm proper from the management of
parallelism (e.g.~process management, communication, synchronization.)  This
separation allowed us to focus on the (sequential) local search algorithm and
on its X10 encoding.
% Parallel experimentations are done at run-time 
Tuning is done by setting the parameters
controlling the parallel execution, for instance \emph{nodes}, \emph{cores
  per node}, \emph{communication scheme}, \emph{delays}.  To test with IW,
 all communications are simply deactivated.  To experiment with CW, the
parameters controlling the cooperation have to be fine tuned.

% To test the IW strategy we simply deactivated all communications while to
% experiment with CW we have to fine tune the parameters controlling the
% cooperation.

\subsubsection{Cooperation parameters} We experimented with the most important
parameters in order to analyze the impact of each one over the global
performance.  Due to space limitations, we do not detail these
experiments\textsuperscript{\ref{fn:supplemental}} -- we only state the
retained parameters.  It turned out that \emph{intensification} is much more
important than \emph{diversification} for SMTI.  The best results are
obtained with 2 \textit{teams}.  The \textit{number of explorers} of a team
is half the number of used cores.  Inside a team, each explorer
periodically exchanges information with the \textit{Elite Pool} according to
the \textit{report interval} ($R$) and \textit{update interval} ($U$).  The
best settings were found to be $R=50$, $U=100$
($\frac{U}{R}=2$ being the best ratio), $|EP| = 4$ and $pAdopt=1$.

\subsubsection{Problem Set} As shown in the previous section, SMTI
problems of size 100 are very easy to solve with sequential AS.
We therefore consider problems of size 1000 generated with
$p1 = 0.95$ and $p2 = 0.8$.  We selected these parameters because the
resulting problems involve a large number of variables, a huge search
space ($1000! \simeq 10^{2567}$) and because they are difficult to
solve due to the high level of incompleteness in the preference
lists. For this experiment, we generated 10 random problems and
executed each one 50 times (the results are averaged), varying the
number of cores from 1 (sequential) to 128.  
%Removing the limit on the number of iterations 
Using an unlimited timeout
forced the solver to discover perfect stable marriages.
%We run the tests using 2, 4, 8, 16, 32, 48, 64, 80, 96, 112 and 128
%cores.   

\subsubsection{Parallel hardware} All parallel experiments have been
carried out on a cluster of 16 machines, each with 4 $\times$ 16-core
AMD Opteron 6376 CPUs running at 2.3 GHz and 128 GB of RAM.  The nodes
are interconnected with InfiniBand FDR $4\times$ (i.e.~56 GBPS.)  We
had access to 4 nodes and used up to 32 cores per node, i.e.~128
cores.  We made no attempt to control thread placement.  In the rest
of this section, the execution times are given in seconds and
correspond to \textit{wall time} which is the real elapsed time, and
includes the time to install all solver instances, the time to solve
the problem, the time for communications and the time to detect and
propagate the termination.
%(time to deploy all X10 places, each solver instance being mapped to a
%place)


\begin{figure}[htb]
%\centerline{\includegraphics[width=0.9\columnwidth]{images/times-N}} 
\centerline{\includegraphics{images/times-N}} 
\caption{Execution time using IW and CW.}
\label{fig:times-N}
\end{figure}
 
Figure~\ref{fig:times-N}\label{fn:log-scale} presents a log-log graph of execution times using IW and CW.
The \textit{Ideal time} corresponds to linear speedup: time is halved
when the number of cores is doubled.
%\footnote{
%\label{fn:log-scale}Both
%  the X and Y-axes use a logarithmic scale.}  
It is worth noticing
that IW is rather efficient: using 128 cores the average time
decreases from \Seconds{44.453} to \Seconds{1.207} which corresponds
to a speedup factor of 37.  However, as is often the case with IW, it
is difficult to obtain a linear speedup.  Moreover, this sub-linear
speedup tends to taper off.  The results with CW are much better. The best recorded speedup is 86 using 80 
cores (\Seconds{44.5} to \Seconds{0.519}).  Beyond this
number of cores, the time tends to stabilize.  This could mean that we
have reached an incompressible limit to solve problems of size 1000,
or that we are being limited by low-level factors.



\subsection{Evaluation on Hard Problems}
We evaluate the benefit of parallelism on hard problems.
%In the previous experiment we noted that some problems are much more
%difficult to solve than most (recall that SMTI is an NP-hard problem).
%We decided to evaluate the benefit of parallelism on those hard
%problems.  
To do so, we generated 100 random problems of size
$n=1000$, ran them sequentially and selected the 10 hardest instances.
We then repeated the previous experiment, using only these instances.

\begin{figure}[htb]
%\centerline{\includegraphics[width=0.9\columnwidth]{images/times-H.pdf}} 
\centerline{\includegraphics{images/times-H.pdf}} 
\caption{Execution time on hard problems}
\label{fig-hard-speed-up}
\end{figure}
 
Figure~\ref{fig-hard-speed-up} presents a log-log graph of the execution time using IW and CW
for these hard problems.%\textsuperscript{\ref{fn:log-scale}}
% Again, the dotted \textit{Ideal time} corresponds to the linear speedup.
We also recall the CW curve obtained in the previous experiment
(i.e. on \textit{normal} problems), for reference.


The difficulty of the problems is clear from the sequential time: in
the previous experiment, a problem was solved in about \Seconds{44}
while about \Seconds{285} are needed for the hardest instances.
Using IW, we reach a quasi-linear speedup: 91.5 for 128 cores which
corresponds to a reduction of the execution time from \Seconds{284.5}
to \Seconds{3.1}.  However the execution time remains much slower than
for \textit{normal} problems.  It turns out that, when using
cooperation in CW, execution times are drastically improved and the
best speedup is 492 with 128 cores corresponding to an execution time
of \Seconds{0.579}.  It is worth noticing that this time is very
similar to the best time (\Seconds{0.519}) obtained in the previous
experiment for normal problems.

From a practical point of view, it appears that parallelism with
cooperation neutralizes the relative difficulty of problems, as
instances which are originally about 6 times harder get solved in
approximately the same time.  Of course, the problem retains its
worst-case NP-hard complexity, and parallel search cannot change this.

% Local Variables:
% mode: tex
% mode: reftex
% TeX-master: "SMTI-AAAI15"
% End:
