\section{Performance Evaluation}
\label{sec:seq-eval}

In this section, we compare our AS modeling of SMTI problems to other
approaches, both from the point of view of performance and that of
solution quality, i.e.~size of a
marriage.
  This
evaluation is important for two main reasons: first, it assesses
whether AS is a useful approach to tackle SMTI problems.
Since~\cite{Gelain2013} it has been known that local search is a
viable way to solve SMTI problems, we will show that AS improves on
that.  Second, the comparison demonstrates the quality of the
sequential implementation, which is important when evaluating the
performance of the parallel version, relative to the sequential
one.\footnotemark% this is for layout: I want to footnote on the right column for this particular page

%
% It is thus significant not to compare to an artificially slow base
% version.
% TODO I do not like this... forward reference.. one can think we use
% parallelism here. To be clarified

To this end, we used an X10 implementation of AS, further discussed in
Section~\ref{sec:parallel-eval}, running sequentially on an AMD
Opteron 6376 clocked at 2.3 GHz, i.e.~using only one core.

\subsubsection{Problem Set}

For the evaluation, we used the random problem generator
described in~\cite{Gent2002} which takes three parameters: 
%size of the problem ($n$), 
the size ($n$), the probability of incompleteness ($p1$) and the
probability of ties ($p2$).
%For each man and woman, the algoritms generates a random
%permutation of size n, as a preference list.
% Then, for each man's
% preference list, $m_i$, and for each woman in his preference list,
% $w_j$, with a probability $p1$ the woman $w_j$ is deleted from $m_i$'s
% preference list and $m_i$ is deleted from $w_j$'s preference list.  If
% any man or woman has an empty preference list, we discard the problem
% and go to step 1.  We iterate over each person’s (men and women’s)
% preference list as follows: for a man, $m_i$ , and for each woman in his
% preference list, in position j ≥ 2, with probability $p2$ , we set the
% preference for that woman as the preference for the woman in position
% j − 1 (thus, putting the two women in a tie).
We generated problems of size $n=100$, with $p1$ ranging over
$[0.1,0.9]$ and $p2$ over $[0,1]$, with step $0.1$.  For
each $(p1,p2)$ pair, we solved 100 instances and averaged the
results.


\subsection{Comparison with Local Search}
\label{sec:comp-with-local}

We first consider the Local Search method LTIU of~\cite{Gelain2013}.
We ran the AS implementation on the test benchmark, in the same conditions as
the LTIU paper: using the same problem generator, solving each instance
once, with a limit of 50\,000 iterations and using only 1 core.
\begin{figure}[htb]
  % \centerline{\includegraphics[width=0.9\columnwidth]{images/ps-vs-p2-Compare-LS-v2}}
  \centerline{\includegraphics[scale=0.95]{images/ps-vs-p2-Compare-LS-v2}}
% \caption{AS vs.~LTIU - perfect stable marriages (no singles).}
  \caption{AS vs.~LTIU - quality of solutions.}
  \label{fig-com-LS-ps}
\end{figure}
 
\begin{figure}[hbt]
  % \centerline{\includegraphics[width=0.9\columnwidth]{images/LTIUvsAS-time-log}}
  \centerline{\includegraphics[scale=0.95]{images/LTIUvsAS-time-log}}
  \caption{AS vs.~LTIU - execution time.}
  \label{fig-com-LS-time}
\end{figure}
 
Figure~\ref{fig-com-LS-ps} compares the quality of solutions.
The percentage of perfect stable marriages found is slightly
better for AS. Moreover, AS does not suffer from the LTIU's dramatic
performance loss when $p2=1$.



Figure~\ref{fig-com-LS-time} compares execution times. The AS implementation
is much faster (thus the log Y scale): the LTIU method averages over \Seconds{30} on a
similar machine~\cite{Gelain2010} while AS is two orders of
magnitude faster.  When $p2$ increases, the lead extends even
further.
%\footnote{This difference in runtimes prompted us to use a
%  logarithmic Y scale for the runtime chart.}

\subsection{Comparison with Approximation Algorithms}
\label{sec:comp-with-mcderm}

We compared AS against McDermid's method (MD)~\cite{Mcdermid2009}, a
very efficient 3/2-approximation algorithm, as implemented in~\cite{Podhradsky2010}.  For MD also, and 
for each $(p1,p2)$ pair, we ran the same 100 instances once, averaging
the execution time\footnotetext{Both the source code and problem
  instances are available at
  \texttt{http://cri-hpc1.univ-paris1.fr/smti/}}.


\begin{figure}[htb]
  % \centerline{\includegraphics[width=0.9\columnwidth]{images/AST300vsMC-PS}}
  \centerline{\includegraphics[scale=0.95]{images/AS50KvsMC-PS}}
% \caption{AS vs.~MD - perfect stable marriages (no singles).}
  \caption{AS vs.~MD - quality of solutions.}
  \label{fig-com-Mc-PS}
\end{figure}


\begin{figure}[hbt]
  % \centerline{\includegraphics[width=0.9\columnwidth]{images/AST30vsMC-3DTime}}
  \centerline{\includegraphics[scale=0.95]{images/AST35vsMC-3DTime}}
  \caption{AS vs.~MD - execution time.}
  \label{fig-com-Mc-Time}
\end{figure}


Figure~\ref{fig-com-Mc-PS} compares the quality of solutions.
%found by both algorithms.  
The percentage of perfect stable marriages found by the
AS algorithm is considerably higher than those found by MD, in
particular using a probability of ties $p2 \in [0.1..0.7]$.

Figure~\ref{fig-com-Mc-Time} compares the execution times, as a 3D chart.  In many cases, AS is up to an
order of magnitude faster than MD.  With higher probability of
incompleteness (e.g. $p1=0.9$), MD outperforms AS.  This can be
explained by the time-complexity of MD which is proportional to the
total length of the preference lists, i.e.~it linearly decreases as
$p1$ increases.
 
We note that MD always returns the same, single and (sub)optimal
solution, while AS will yield more than one solution, with observably
better quality.
                                %%
% While MD is t-polynomial on the length of the preference list, ...
                                %%
Moreover, a \emph{solution quality} vs.~\emph{performance} trade-off
is always possible in AS, by tweaking the timeout parameter.

% may always be attained at the cost of
% solution quality, by lowering the timeout parameter of AS.


% In order to compare the behavior of both algorithms with bigger
% instances, we develop other experimentation, but now we use SMTI
% problem instances of size 1000. Using the same problem generator as
% the described in the test above, we create random SMTI problems of
% size 1000 with a probability $p1$ varying in [0.1,~0.9] with
% step of 0.1 and a probability $p2$ varying in [0,~1] with step
% 0.2. We tested 100 problem instances for each combination of the
% parameters.

\subsection{Comparison with SAT}
\label{sec:comparison-with-sat}

We also compare AS to the SAT encoding for SMTI of~\cite{Gent2002a},
restricting thus to the decision problem: \emph{is there a stable matching of
  size $n$?} which we answer by actually finding a perfect stable matching.

\begin{figure}[htb]
  %\centerline{\includegraphics[width=0.9\columnwidth]{images/ASvsSAT-log}
  \centerline{\includegraphics[scale=0.95]{images/ASvsSAT-log}}
  \caption{AS vs.~SAT - execution time ($p1 = 0.5$).} 
 \label{fig:com-SAT-Time}
\end{figure}

Figure~\ref{fig:com-SAT-Time} presents the execution times using weak
stability, for $n = 100$, i.e.~the case of figure~2, top left
from~\cite{Gent2002a}.  The execution times for SAT were divided by
25.2, according to the ratio of the SPECint CPU performance ratings of
both machines but, even so, AS outperforms the SAT version by a factor
of about 50.

% Local Variables:
% mode: tex
% mode: reftex
% TeX-master: "SMTI-AAAI15"
% End:
