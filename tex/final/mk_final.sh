#!/bin/bash

# This script creates the latex package for the final submission of a paper
#
# Copyright (C) 2014 Daniel Diaz (Daniel.Diaz [at] univ-paris1.fr)
# Creation     date: 2014 Nov 16
# Modification date: 2014 Nov 18
# 
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version. 
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.

version='1.0'
copyright='Copyright (C) 2014 Daniel Diaz'

prog=$( basename $0 )

export LC_ALL=C
export LANG=C


driver=pdflatex
expand_bbl=0
copy_bib=0
copy_min_sty=1
verbose=0
tmp_prefix=__tmp
tmp_tex=$tmp_prefix-tex.tex
graphics_file=$tmp_prefix-grx
dist=Distribution
res_tex=
flat_graphics=0
purge=0

title_echo() {
    echo
    echo "*** $* ***"
}

verb_echo() {
    [ $verbose -gt 0 ] && echo "    $*"
}

filter_echo() {
 while read line; do echo "    $line"; done
}

error() {
    echo "ERROR: $1" >&2
    exit ${2:-1}
}

run() { 
    verb_echo "\$ $@"
    "$@"
    ret=$?
    [ $ret = 0 ] || error "command [$*] failed with code: $ret" $ret
    return 0
}

run_latex() {
    verb_echo "\$ $driver $1"
    if ! $driver -halt-on-error -interaction=errorstopmode $1 </dev/null >$tmp_tex_output; then
	tail $tmp_tex_output | filter_echo
	error "command $driver $1 - see $tmp_tex_output"
    fi
}

check_exist() {
    [ -f "$1" ] || error "cannot find file: $1"
}


ensure_suffix() {
    echo ${1/$2}$2
}

del_suffix() {
    echo "$1" | sed 's/\.[0-9a-zA-Z_][0-9a-zA-Z_]*$//'
}

# get_latex_files_in_cmd FILE MACRO EXTENSION
get_latex_files_in_cmd() {
    files=
    for f in $(sed -ne 's/^\\'$2'{\([^}]*\)}.*$/\1/p' $1 | tr ',' ' '); do
	files="$files "$( ensure_suffix "$f" $3 )
    done
    echo $files
}

while [ x"$1" != x ]; do
    case "$1" in
	-latex|--latex) 
	    driver=latex;;

	-pdflatex|---dflatex) 
	    driver=pdflatex;;

	-o|--output)
	    shift
	    [ -z "$1" -o "${1:0:1}" = - ] && error "missing filename after -o"
	    res_tex=$1;;

	-dir|--dir)
	    shift
	    [ -z "$1" -o "${1:0:1}" = - ] && error "missing directory after -dir"
	    dist=$1;;

	-purge|--purge)
	    purge=1;;

	-expand-bbl|--expand-bbl)
	    expand_bbl=1;;

	-copy-bbl|--copy-bbl)
	    expand_bbl=0;;

	-copy-bib|--copy-bib)
	    copy_bib=1;;

	-copy-min-sty|--copy-min-sty)
	    copy_min_sty=1;;

	-copy-all-sty|--copy-all-sty)
	    copy_min_sty=0;;

	-flat-graphics)
	    flat_graphics=1;;

	-no-flat-graphics)
	    flat_graphics=0;;

        -v|--verbose) 
	    verbose=1;;

	-version|--version)
	    echo "$prog version $version"
	    echo $copyright
	    exit 0;;

	-h|-help|--help) 
	    echo "Usage: $prog [OPTION]... TEX_FILE"
	    echo
	    echo "OPTION:"
	    echo
	    echo "    -pdflatex         use the pdflaex driver (default)"
	    echo "    -latex            use the latex   driver"
	    echo
	    echo "    -o TEX_FILE       name of the resulting TeX file (in DIR)"
	    echo "    -dir DIR          put resulting files in DIR (default: $dist)"
	    echo "    -purge            purge DIR at the beginning"
	    echo
	    echo "    -expand-bbl       expand the .bbl file"
	    echo "    -copy-bbl         copy   the .bbl file (default)"
	    echo "    -copy-bib         also copy needed .bib and .bst files"
	    echo
	    echo "    -flat-graphics    flatten graphic files - experimental ! use with care"
	    echo "    -no-flat-graphics respect graphic file directories (default)"
	    echo
	    echo "    -copy-min-sty     only copy .sty files used and present here (default)"
	    echo "    -copy-all-sty     only all .sty files used"
	    echo
	    echo "    -v                verbose"
	    echo "    -version          show version number and exit"
	    echo "    -h                show this help and exit"
	    exit 0;;

	-*)
	    error "Unknown option $1";;


        *)  if [ x"$src_tex" = x ]
            then
                src_tex=$1
            else
	       error "TeX source file $src_tex already given"
	    fi
    esac
    shift

done


[ -z "$src_tex" ] && error "TeX source file missing (use -h for help)"

src_tex=$( ensure_suffix $src_tex .tex )
check_exist "$src_tex"

[ -z "$res_tex" ] && res_tex=$src_tex
res_tex=$( ensure_suffix $res_tex .tex )

tmp_tex_output=`pwd`/$tmp_prefix.texout

dist_src_tex=$dist/$res_tex

title_echo "$prog version $version"

title_echo "Flattening $src_tex into $tmp_tex"

# in case of problems with latexpand try to add --empty-comments 

run latexpand -o $tmp_tex $src_tex
    
title_echo "Creating the target directory $dist"

[ -d $dist -a $purge = 1 ] && run rm -rf $dist

[ -d $dist ] || run mkdir $dist

title_echo "Compiling $tmp_tex to gather information on needed packages (.sty)"

run_latex $tmp_tex 

title_echo "Copying needed packages to $dist"

packages=$(sed -n 's/^.*(\(.*\.sty\)/\1/p' ${tmp_tex/.tex}.log | sort | uniq)
for i in $packages; do
    if [ $copy_min_sty = 0 -o ${i:0:1} = . ]; then
	run cp $i $dist
    fi
done

title_echo "Checking bibliography"
bibfiles=$( get_latex_files_in_cmd $tmp_tex bibliography .bib )

bblfile=
bblexp_opt=

if [ ! -z "$bibfiles" ]; then
    verb_echo "$src_tex refers bibfiles: $bibfiles"
    bblfile=${src_tex/%.tex}.bbl
    check_exist "$bblfile"
    if [ $expand_bbl = 0 ]; then
	run cp "$bblfile" $dist/${res_tex/%.tex}.bbl
    else
	bblexp_opt="--expand-bbl $bblfile"
    fi
    if [ $copy_bib = 1 ]; then
	for file in $bibfiles; do
	    check_exist "$file"
	    run cp "$file" $dist/.
	done
	bstfiles=$( get_latex_files_in_cmd $tmp_tex bibliographystyle .bst )
	[ -z "$bstfiles" ] || verb_echo "$src_tex refers bib stlye files: $bstfiles"
	for file in $bstfiles; do
	    check_exist "$file"
	    run cp "$file" $dist/.
	done
    fi
fi


title_echo "Generating $dist_src_tex"

run latexpand -o $dist_src_tex --show-graphics $bblexp_opt $src_tex  2>$graphics_file

title_echo "Copying needed figures to $dist"
for path in $(cat $graphics_file); do
    dir=$(dirname $path)
    distdir=$dist/$dir
    file=$(basename $path)
    if [ $flat_graphics = 0 ]; then
	[ -d $distdir ] || run mkdir -p $distdir
	run cp $path $dist/$path
    else
	path_dst=$dist/$basename
	if [ x$dir != x -a $dir != . -a $dir != ./ ]; then
	    [ ${path:0:2} = ./ ] && path=${path:2}
	    path1=$( del_suffix "$path" )
	    file1=$( del_suffix "$file" )
	    verb_echo "replace $path by $file"
	    sed -e "s%\(includegraphics[^{]*{ *\)$path1%\1$file1%g" $dist_src_tex >$dist_src_tex-1
	    mv $dist_src_tex-1 $dist_src_tex
	fi
	run cp $path $dist
    fi
done

title_echo "Compiling the new file $dist_src_tex"

run cd $dist

verb_echo "First time..."

run_latex $res_tex

verb_echo "Second time for references..."

run_latex $res_tex
[ $verbose -gt 0 ] && tail -2 $tmp_tex_output | filter_echo
    
run rm -f ${src_tex/%.tex}.{log,out}

run cd ..

run rm -f "$tmp_prefix"*

echo
echo "SUCCESS: you can open ${dist_src_tex/%.tex}.pdf"
echo
exit 0

