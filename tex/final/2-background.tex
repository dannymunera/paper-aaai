\section{Background}
\label{sec:background}

\subsection{Stable Marriage with Ties and Incompleteness}
\label{sec:defs}

We recall the main definitions for SMTI problems~\cite{Scott2005,Iwama2008}.

\begin{defn}
(SMTI problem) 
An SMTI instance of size $n$ consists of $n$ men and $n$ women,
and a preference list for each of them, which contains some of the people of the other gender.
Such preference lists are weak orders, that is, total orders possibly containing ties.
\end{defn}
While not required by the theory we assume that the preference list
for woman $w$ does not contain man $m$ iff the list for $m$ does not
contain $w$. This is trivial to establish with a pre-pass; the
generator we use \cite{Gent2002} already ensures this.   
\begin{defn}
  \label{def:marriage}
  (Marriage) 
  Given an SMTI instance, a \emph{marriage} $M$ is a set of pairs $(m,w)$ representing a
  (possibly partial) one-to-one matching of men and women.  
  % vj: this is obvious since M is a set.
  % We denote by $(m,w) \in M$
  %the fact that a man $m$ and a woman $w$ are matched in $M$.  
  If a man $m$ is not matched in $M$ (i.e.{} for no $w$ is it the
  case that $(m,w)\in M$), we say that  $m$ is
  \emph{single} in $M$ (similarly for women). The  \emph{size} of a marriage $M$ is the
  cardinality of $M$. % (the set) $M$. 
%is its cardinal $|M|$ which amounts to the
%  number of men (or women) that are married.
\end{defn}

With the introduction of ties in the preference lists, three different
notions of stability may be
used~\cite{DBLP:journals/dam/Irving94,DBLP:journals/tcs/ManloveIIMM02,Iwama2008}.
As we only consider \emph{weak stability} (the most challenging), we
simply call it stability.  We define \emph{blocking pair} in this
context:

\begin{defn}
  \label{def:blocking-pair}
  (Blocking Pair) 
  In a marriage $M$, $(m,w)$ is a \emph{Blocking Pair} (BP)
  iff (a)~$m$ and $w$ accept each other and (b)~$m$ is either single 
  in $M$ or strictly prefers $w$ to his current wife, and (c)~$w$ is either single
  in $M$ or strictly prefers $m$ to her current husband.
\end{defn}

\begin{defn}
  \label{def:stable-marriage}
  (Stable marriage) 
  Given an SMTI problem instance, a marriage $M$ is stable iff it has no
  blocking pairs.
\end{defn}

A (weakly) stable marriage always exists and can be found with
variants of the GS algorithm. 
% vj: already introduced GS with citation in Section 1.
%~\cite{GaleShapley1962}.
Since any given SMTI instance may have
stable matchings of different sizes, a natural requirement is to find
those of maximum cardinality.  This optimization problem (called
MAX-SMTI) has many real-life
applications~\cite{Iwama2008,DBLP:journals/tcs/ManloveIIMM02} and has
attracted a lot of research in recent years.  The MAX-SMTI problem has
been shown to be NP-hard, even for very restricted cases (e.g.{} only
men declare ties, ties are of length two, the whole list is a
tie)~\cite{DBLP:conf/icalp/IwamaMMM99,DBLP:journals/tcs/ManloveIIMM02}. 
For brevity, in the rest of this article we refer to MAX-SMTI
as SMTI.

\subsubsection{SMTI algorithms}

Finding efficient algorithms to solve SMTI has been an active field of
research for several years, driven by its applications.  SMTI has been
shown to be APX-hard~\cite{DBLP:journals/tcs/HalldorssonIIMMMS03} and
most of the recent research focuses on designing efficient
\emph{approximation algorithms}, i.e. algorithms running in polynomial
time yet able to guarantee solutions within a constant factor of the
optimum~\cite{Kiraly11}.
% Recently, several algorithms have been proposed to improve this
% factor~\cite{Kiraly11,DBLP:journals/algorithms/Kiraly13}.
Currently, the best algorithms are 3/2-approximation
algorithms~\cite{Mcdermid2009,DBLP:journals/algorithms/Kiraly13,DBLP:journals/algorithms/Paluch14}.
An $r$-approximation algorithm always finds a stable matching $M$ with
$|M| \geq |M_{opt}| / r$ where $M_{opt}$ is a stable marriage of
maximum size.  SMTI cannot be approximated within a factor 21/19 and
probably not within a factor of 4/3
either~\cite{DBLP:journals/talg/HalldorssonIMY07}.
These algorithms only find one solution for a given
problem, while it is often useful to
%can be very useful, in several situations, to
provide multiple (quasi-)optimal solutions.
% In this article we will compare our sequential algorithm with McDermid: an
% efficient 3/2-approximation algorithm.

Constraint Programming (CP) can be used to solve SMTIs.
While there are some efforts to solve SM
problems~\cite{Gent2001}, including the introduction of new global
constraints to achieve efficient
consistency~\cite{DBLP:journals/corr/UnsworthP13,DBLP:conf/cpaior/ManloveOPU07}, few papers are devoted to
the MAX-SMTI variant.  The reference
paper~\cite{Gent2002} is more a study of the properties of SMTI
problems of limited size (i.e.~$n \leq 60$) than a search for
efficient encodings to solve SMTI using CP.

Surprisingly, SAT solvers have not been extensively used for SMTI.
The work by Gent~\cite{Gent2002a} proposes a SAT encoding for SMTI and uses the
Chaff solver for its evaluation.
%  In this article we will compare our sequential solver with this evaluation.
Linear programming~\cite{roth_stableLP:MOR93,VadeVate89} and integer
programming~\cite{DBLP:journals/corr/KwanashieM13} have been also studied for
variants of SM like the Hospitals/Residents problem.  Recently, Local
Search has been successfully applied to SMTI.  In~\cite{Gelain2013}, the
authors propose LTIU, a local search algorithm with very good
performance. Comparisons with some of these papers are in
Section~\ref{sec:seq-eval}. 
%Obviously, we wil compare our sequential algorithm to LTIU.


\subsection{The Adaptive Search method}
\label{sec:background-AS}
Adaptive Search (AS) was proposed in~\cite{CodognetD01} as a generic,
domain-independent, constraint-based local search method.  This
meta-heuristic takes advantage of the model of the problem in terms of
constraints and variables in order to guide the %optimization
search more precisely than a single global cost function.
%(e.g., the number of  violated constraints). 
%The  core ideas of AS can be summarized as follows:
AS starts from a random assignment of the variables (i.e. a
\emph{configuration}) and, iteratively, tries to improve it, modifying
one variable at a time until a solution is found.  To this end AS:

\begin{itemize}

\item Needs to model the constraints with heuristic functions that
  compute an approximate degree of satisfaction of the goals (the
  current \emph{error} on the constraint);

\item Combines these errors to compute the global cost of a
  configuration;

\item For each variable, aggregates the errors of constraints in which
  it occurs, and repairs the \emph{worst} variable (highest error)
  with the most promising value;

\item maintains a short-term memory (e.g. \emph{tabu
    list}) of recently modified variables which led to local minima,
  together with a reset mechanism (i.e. \emph{iterative local search}).

\end{itemize}

AS has shown good performance on combinatorial problems such as
classical CSPs, and the Costas Array
Problem~\cite{CaniouCRDA:2014:Constraints}.


% Local Variables:
% mode: tex
% mode: reftex
% TeX-master: "SMTI-AAAI15"
% End:
