\section{An Adaptive Search Model for SMTI}
\label{sec:AS-model}

%In this section we first model SMTI as a permutation problem and then propose
%an AS modeling.
%% In this section we present an AS modeling of the SMTI problem.
%AS is one of the solving methods available in the {\coop} framework, which
%specializes the general AS method for permutation problems, i.e.~ones
%involving $n$ variables ranging over $1\ldots n$, subject to an
%\texttt{all-different} constraint.
%% In this variant, the initial configuration is a random permutation
%% of the domain.
%Assigning a variable implies swapping its value with that of some
%other variable.
% the values of 2 variables, to ensure the \texttt{all-different}
% constraint holds.

We model SMTI in AS as a permutation problem: the sequence of $n$
$(X_1 \ldots X_n)$ takes on as values permutations of the values
$1\ldots n$ (implementing an \texttt{all-different} constraint).
$X_i=j$ is interpreted as either $(m_i,w_j)\in M$, or $m_i$ is single
if $w_j$ is not on its preference list.  Note this interpretation
remains valid when the values of any two variables are swapped (this
is how value assignment is implemented in permutation problems).

%To model SMTI in AS, 
%we encode a marriage $M$ as a vector of $n$
%variables $(X_1 \ldots X_n)$ 
%denoting the woman associated with each man:
%$X_i=j$ means that $(m_i, w_j) \in M$.  Since a woman can only be married to
%one man at a time, the vector forms a permutation of the values $1..n$.  Due
%to possible incomplete preference lists, some men may be single.  Instead of
%assigning a special value to encode celibacy, a man is single iff his matched
%woman is not in his preference list.  This allows us to model SMTI as a
%permutation problem  i.e.~one
%involving $n$ variables ranging over $1\ldots n$, subject to an
%\texttt{all-different} constraint. 
%Assigning a variable implies swapping its value with that of some
%other variable.

To improve stability of a marriage, we must remove blocking pairs
(BPs). Some BPs may be useless in that fixing them does not improve
things since the man involved remains part of another BP. 
%To check the stability of a marriage we rely on the notion of blocking
%pairs (BP), as per definition~\ref{def:blocking-pair}.  Although the
%number of BPs can get very large, many of them are hopefully useless,
%in that fixing them does not improve things: the men involved remain
%part of other BPs.  
We thus focus on the so-called \emph{undominated
  blocking pairs}~\cite{DBLP:journals/geb/KlijnM03,Gelain2013}.

\begin{defn}
  \label{def:dominated-blocking-pair}
  (Dominated blocking pair) BP $(m,w)$ dominates BP $(m,w')$ iff $m$
  prefers $w$ to $w'$.
\end{defn}

\begin{defn}
  \label{def:undominated-blocking-pair}
  (Undominated blocking pair) BP $(m,w)$ is undominated iff there is
  no other BP dominating $(m,w)$.
\end{defn}

These definitions are from the men's point of view: equivalent ones
exist for women.  From an implementation perspective, finding the
undominated BP of a man $m$ amounts to consider each woman $w$ in his
preference list in descending order of preference, stopping at the
first BP encountered: $(m,w)$ is then an undominated BP.  In the
following we only consider \emph{undominated} BPs, which we simply
call BPs.

The cost function of a marriage measures both its stability (number of BPs)
and its quality (number of singles).  Hence: $cost(M) = \#BP(M) \times n +
\#Singles(M)$, where $\#BP(M)$ is the number of BPs in $M$, and
$\#Singles(M)$ is the number of singles in $M$.  The number of BPs is weighted
with $n$ to prioritize stable marriages over marriages with fewer
singles.  A marriage $M$ is stable iff $cost(M) < n$, and {\em
  perfect} iff $cost(M) = 0$. AS stops as soon as the cost function
reaches 0 or when a given time limit is hit, in which case it returns the
best marriage found so far.

We define $R(w,m)$ as the \textit{rank} of $m$ in the
preference list of $w$, ranging over $1..(n+1)$, with $i<j$ implying
$w$ prefers (man with rank) $i$
to (man with rank) $j$, and $R(w,m)=n+1$ iff $m$ is not in the preference list of $w$. 
Suppose $(m,w)$ and $(m',w') \in M$ form a BP $(m,w')$, then 
the error for $X_m$ (in $M$) is $R(w',m')-R(w',m)$.  Thus,
the further the assigned man is from the BP, the larger the
error.  Algorithm~\ref{algo:bp-error} has details -- 
it is worth pointing out that 
when $w'$ is single in $M$,
%$R(w',m)=n+1$ ($w'$ is single in $M$) 
the returned score
could be $R(w',m')$; this often over-estimates the importance of
this case and we have empirically found that $1$ is a
better choice.  Note that the actual implementation does some straightforward
pre-computation to avoid the linear cost of recomputing $R(w,m)$.
%(Note the actual code pre-computes
%  a reverse matrix which records for each $w$ and $m$ the rank of $m$
%  in the preference list of $w$. This can be done in place, and avoids
%  the ostensible linear cost of computing $R(w,m)$.)

\begin{algorithm}[htb]
  \caption{Function to compute BP error}
  \label{algo:bp-error} 
  \begin{algorithmic}[1]
   \Require{$(m',w') \in M$ and a man $m$ who prefers $w'$ to his partner}
   \Ensure{if $(m,w')$ is a BP, return an error $>$ 0 else return 0}
%   \Statex
  \Function{BP\_Error}{$m'$,$w'$,$m$}
 %\Comment{error if $(m,w')$  forms a BP}
   \State $rankM' \gets R(w',m')$ \Comment{rank of current partner of $w'$}
   \State $rankM~ \gets R(w',m)$ \Comment{rank of $m$ in pref.{} list of $w'$}
    \If{$rankM = n+1$}\Comment{$m \notin$ pref.{} list of $w'$}
    \State\Return{0}\Comment{$m$ not a valid partner for $w'$ : not a BP}
    \EndIf
    \If{$rankM' = n+1$}
    \State\Return{1}\Comment{$w'$ is single ($m'$ is not in her list) : BP}
    \EndIf
   \LineCommentCont{using the pref.{} list of $w'$, the error is the difference
     between the rank of her partner $m'$ and proposed man
      $m$}
    \State\Return{$max(0,rankM' - rankM)$}
   \EndFunction
  \end{algorithmic}
\end{algorithm}
 
At each iteration, AS selects the ``worst'' variable from the current
marriage $M$ to improve it.  If several variables have the same error, AS randomly
picks one.  AS then fixes the culprit by swapping $X_m$ and $X_{m'}$. 
In short, AS considers all BPs, chooses the variable corresponding to the
worst one, fixes it by moving to a new configuration and re-evaluates the
cost of the resulting marriage.  This heuristic avoids the cost of fixing all
BPs, one by one.

Since both the evaluation of the cost function and the evaluation
of variable errors require the computation of the blocking pairs,
when the cost function is evaluated, the error
on variables can also be computed and tabled for later use.  This is
shown in Algorithm~\ref{algo:cost}.

\begin{algorithm}[htb]
  \caption{Function to evaluate a marriage}
  \label{algo:cost} 
  \begin{algorithmic}[1]
   \Require{$M$ the marriage to evaluate}
   \Ensure{the global cost and error on variables}
%   \Statex
   \Function{\textsl{\textsc{Cost\_Of\_Marriage}}}{$M$}
      \State $nSingles \gets 0$
      \For{$m \gets 1$ \textbf{to} $n$}
         \State $w \gets X_m$ \Comment{$(m,w) \in M$ or $m$ is single}
         \State $rankW \gets R(m,w)$ \Comment{rank of $w$ in pref.  list of $m$}
         \If{$rankW = n + 1$}\Comment{$m$ is single}
            \State $nSingles \gets nSingles + 1$
%            \State $rankW \gets n + 1$\Comment{to check all women for $m$}
         \EndIf
         \For{all $w' \in$ pref list of $m$ with rank $< rankW$}
            \State \textbf{let} $(m',w') \in M$ \Comment{$m'$ is the partner of $w'$
              in $M$}
\LineComment{check if $(m,w')$ forms a BP}
            \State $error_m \gets $ \textsc{BP\_Error}$(m',w',m)$
            \If{$error_m > 0$}
               \State $\#BP \gets \#BP + 1$
               \State \textbf{break} \Comment{only consider undominated BP} 
            \EndIf
         \EndFor
      \EndFor
      \State\Return{$\#BP \times n + nSingles$}
   \EndFunction
  \end{algorithmic}
\end{algorithm}


In most cases, the resulting marriage improves on the current one and
AS continues iteratively.  When this is not the case, AS has reached a
minimum (global or local).  As AS has no way of knowing when
the optimum has been reached (except when the cost is 0) it handles both
cases similarly trying to escape the minimum.
%both
%situations are handled similarly and AS tries to escape the
%minimum.  
To this end, AS relies on a Tabu list to prohibit the use of
%the most 
recent culprit variables.  When the list becomes too large,
AS invokes a \emph{reset procedure} to alter the current configuration.
% in order to escape the local minimum. 
 The Tabu mechanism is not used for
SMTI: as soon as a local minimum is reached a customized reset
procedure is invoked which basically tries to fix the 2 worst BPs
and/or to assign a woman to a single man, as detailed in
Algorithm~\ref{algo:reset}.  This procedure is stochastic; it takes a
probability $p$ to \emph{also} fix the second worst variable: good
results are obtained with a high probability, e.g.~$p \simeq 0.98$.
This procedure turns out to be very effective: while preserving most
of the configuration (no more than 2 swaps are performed), it enables
AS to escape all local minima and reach very good solutions.


\begin{algorithm}[htb]
  \caption{Reset procedure to escape local minima}
  \label{algo:reset} 
  \begin{algorithmic}[1]
   \Require{$M$ the marriage currently trapped in a local minimum and $p$ the
     probability to also fix the second worst variable}
   \Ensure{$M$ the perturbed marriage to escape the local minimum}
  \Procedure{Reset}{$M$, $p$}
   \If{$\#BP(M) \ge 1$}
     \State fix the worst variable \Comment{ie. 1 swap}
    \If{$\#BP(M) \ge 2$ and with a probability $p$}
     \State fix the second worst variable \Comment{ie. 1 swap}
     \State \Return \Comment{exit the procedure}
    \EndIf
    \EndIf
   \If{$nSingles(M) \ge 1$}
     \State randomly select a single man
     \State and assign him a random woman \Comment{ie. 1 swap}
   \Else
    \State randomly swap 2 variables \Comment{ie. 1 swap}
   \EndIf
   \EndProcedure
  \end{algorithmic}
\end{algorithm}


% Local Variables:
% mode: tex
% mode: reftex
% TeX-master: "SMTI-AAAI15"
% End:
