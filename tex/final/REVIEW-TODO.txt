----------------------- REVIEW 1 ---------------------
However, one major concern is about the reproducibility of the results. With the description provided in the paper, it is quite difficult to achieve an implementation that is similar to the one reported in the paper (personally, I would have used the option for supplementary material to include more details on the model you construct for SMTI in AS.) In the footnote 2, it is said that "the problem instances used will be made available when authors' identities can be revealed". This is good, but I will also include, for each instance the quality of the solution obtained and the time used (specifying the CPU) by your approach. 

   >>>>>> ANSWER
   we have put a URL to http://cri-hpc1.univ-paris1.fr/smti/ a page with a link
   to the source (on github) and another to the instances (on site.)

   Maybe we should include most of the supplemental material here, or	
   would this be construed as cheating by AAAI?


It would be good to hear more about the generalization of this work to other matching problems (it is only mentioned in the conclusions). On large problems, its size is so high that they do not model any longer real-world problems. 

   >>>>>> ANSWER
   answered in the introduction (cf. REVIEW 2 - remark 1)

----------------------- REVIEW 2 --------------------- 

However, it would have been nice if the authors took a few sentences to better motivate the study of this problem beyond simple academic interest. The authors state that the paper "has many real-life applications and has attracted a lot of research in recent years" while providing a couple references. The provided references don't make the real-world applications immediately obvious either. It would be more compelling to directly connect this problem to the real world in the narrative of this paper rather than suggesting the reader go read other papers to figure out why this problem is interesting.

   >>>>>> ANSWER
   intro now has a paragraph about real applications. 

It would have been nice to have a paragraph or two giving a broad comparison between this Adaptive Search heuristic and the LTIU Local Search method with some insight from the authors as to why this local search (adaptive search) is dominant over the LTIU local search heuristic.

   >>>>>> ANSWER
   Not adressed (we do not have enough space in this paper - do it in the journal version)

it wasn't clear to me why CW was superior to IW on these instances. Is CW typically better than IW or is the advantage and the super linear speedup seen specific to this problem and this local search heuristic. If so, what is it about AS and this problem that make CW effective?

   >>>>>> ANSWER
  Added a sentence in the conclusion to say that we plan to investigate why CW is effective

----------------------- REVIEW 3 ---------------------

-------------------------  METAREVIEW  ------------------------
The reviewers in general liked this paper. 

Please spend some time trying to take on board their comments about making the implementation and results reproducible, etc.

   >>>>>> ANSWER
   see above (URL)

And put any materials they requested that won't fit into the AAAI page limits onto a longer, online version of the paper (available on CoRR/arXiv.org).

   >>>>>> ANSWER
   we prefer a journal version.   
 
I'd encourage you also to buy some extra pages!

   >>>>>> ANSWER
   We prefer a journal version



------------------- OUR PREVIOUS ANSWER ---------------------

We thank the reviewers for their comments. We believe we can adequately follow their suggestions and respond to their questions in the final version of the paper.

Here we just respond to some of the concerns:

- We intend to release the code as open-source, so the results will be reproducible, as the instances we used will also be made available on a web site, together with the BKS (best known solution) obtained thus far.

- We are currently adapting this code to tackle other variants of the stable matching problem, such as HRTI (hospital resident problem with ties and incompleteness). The difference is that there are capacities on one side (hospitals may host several doctors). SMTI can be seen as a special case of HRTI. We hope to be able to show results at the conference, if the paper is accepted.

- The doctors/hospitals or students/schools problems may have a large size indeed, as there are instances at the national level: for instance the Scottish Foundation Allocation Scheme (SFAS) involves instances with over 700 residents and posts. There are similar programs for much larger countries, such as National Resident Matching Program (NRMP) in the USA. Matching problems may also be found in other settings, such as car sharing or bipartite markets with a very large set of consumers and producers.

- Concerning parallelism, CW is not always better than IW: sometimes it even gets worse, because of communication overhead.  Even when it is, it may not be so much as shown in the paper. We are currently studying which features of the stable matching problem make it so suitable for CW.

