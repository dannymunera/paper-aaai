\section{Stable Matching Problems}
\label{sec:background}

This section concerns the definitions we are dealing with:
\emph{stable matching}, \emph{hospitals/residents}, how they may be
encoded and difficulties in solving them.

\subsection{The SMTI Problem}

We recall the main definitions for SMTI problems~\cite{Scott2005,Iwama2008}.

\begin{defn}
  (SMTI problem) An SMTI instance of size $n$ consists of a set of $n$ men
  ($\M$) and a set of $n$ women ($\W$), and a preference list for each of
  them, which contains some of the people of the other gender.  Such
  preference lists are weak orders, that is, total orders possibly containing
  ties. If the preference list of a person $p$ includes a person $q$, we say
  that $q$ is \emph{acceptable} to $p$ (or, sometimes, $p$ accepts $q$).
\end{defn}

While not required by the theory we assume that the preference list for woman
$w$ does not contain man $m$ iff the list for $m$ does not contain
$w$. This is trivial to establish with a pre-pass and the generators we are
using already do this. The preference lists are formalized with the notion of
\emph{rank} via a \emph{ranking function} $R$ (for simplicity we use the same
name $R$ for the ranking function for men and women).

\begin{defn} (Ranking function) $R : \M \times \W \to \{1 \ldots n+1\}$ and $R
  : \W \times \M \to \{1 \ldots n+1\}$, such that $R(m,w)$ is the \emph{rank}
  of $w$ in the preference list of $m$, ranging over $1..(n+1)$, with $i<j$
  implying $m$ prefers (woman with rank) $i$ to (woman with rank) $j$, and
  $R(m,w)=n+1$ iff $w$ is not acceptable for $m$. Similarly $R(w,m)$ captures
  the rank of $m$ in the preference list of $w$ 
\end{defn}

An SMTI instance is therefore specified by two sets $\M$
and $\W$, both of cardinality $n$ and a ranking function $R$.

\begin{defn}
  \label{def:matching}
  (SMTI - Matching) Given an SMTI instance, a \emph{matching} $M$ represents
  a (possibly partial) one-to-one matching of men and women.  $M$ is a set of
  pairs $(m,w)$ such that $m$ and $w$ are acceptable to each other. If a man
  $m$ is not matched in $M$ (i.e.~for no $w$ is it the case that $(m,w)\in
  M$), we say that $m$ is \emph{single} in $M$ (similarly for women). The
  \emph{size} of a matching $M$ is the cardinality of $M$. A matching $M$ is
  said \emph{perfect} if it has no singles ($|M|=n$).
\end{defn}

With the introduction of ties in the preference lists, three different
notions of stability may be used: \textit{weak stability},
\textit{strong stability} and \textit{super
  stability}~\cite{Irving1994,Manlove2002}.  It is known that there
always exists a weak stable matching for any SMTI
problem~\cite{Irving1994,Iwama2008}, such is not the case for the
other two definitions of stability.  Therefore, weak stability is
normally the most sought after property.  For these reasons
%As
we only consider weak stability
%(the most challenging)
and simply call
it \emph{stability}.  We define a \emph{blocking pair} in this
context:

% Vijay: We should add a sentence or two about why we only consider
% weak stability. Are the other forms also amenable to our approach,
% we plan to look at them in future work?

\begin{defn}
  \label{def:blocking-pair}
  (SMTI - Blocking Pair) Given a matching $M$, $(m,w)$ is a
  \emph{blocking pair} (BP) iff (a)~$m$ and $w$ accept each other
  and (b)~$m$ is either single in $M$ or strictly prefers $w$ to
  his current wife, and (c) ~$w$ is either single in $M$ or strictly
  prefers $m$ to her current husband.
\end{defn}

It is worth noticing that some BPs may be useless in that fixing them does
not improve things since the man involved remains part of another BP.
We thus focus on the so-called \emph{undominated blocking
  pairs}~\cite{Klijn2003,Gelain2013}.

\begin{defn}
  \label{def:dominated-blocking-pair}
  (Dominated blocking pair) BP $(m,w)$ dominates BP $(m,w')$ iff $m$
  prefers $w$ to $w'$.
\end{defn}

\begin{defn}
  \label{def:undominated-blocking-pair}
  (Undominated blocking pair) BP $(m,w)$ is undominated iff there is
  no other BP dominating $(m,w)$.
\end{defn}

These definitions are from the men's point of view: equivalent ones
exist for women. 


\begin{defn}
  \label{def:stable-matching}
  (SMTI - Stable matching) Given an SMTI problem instance, a matching
  $M$ is stable iff it has no blocking pairs.
\end{defn}

A (weakly) stable matching always exists and can be found with variants of
the GS algorithm.  Since any given SMTI instance may have stable matchings of
different sizes, a natural requirement is to find those of maximum
cardinality. This optimization problem is called MAX-SMTI; for brevity
we will call it SMTI in the rest of this article.

% This optimization problem (called MAX-SMTI) has many real-life
% applications~\cite{Iwama2008,Manlove2002} and has attracted a lot of
% research in recent years.  The MAX-SMTI problem has been shown to be NP-hard,
% even for very restricted cases (e.g.~only men declare ties, ties are of
% length two, the whole list is a tie)~\cite{Iwama1999,Manlove2002}.  For
% brevity, in the rest of this article we refer to MAX-SMTI as SMTI.

%%
Actually, there are significant applications which look like SMTI but
in which the two sets $\M$
and $\W$
will be of different size, bringing about an ``asymmetric'' SMTI
problem.
% Note that the only increase of generality over the standard SMTI
% formulation is that $m$
% is not required to be identical to $n$.

\begin{defn}\label{def:ASMTI}
  (Asymmetric SMTI problem) An asymmetric SMTI instance is specified by (a)
  two sets $\M$ and $\W$ of cardinality $m$ and $n$ respectively, (b) a
  ranking function $R : \M \times \W \to \{1 \ldots n+1\}$ and $R : \W \times
  \M \to \{1 \ldots m+1\}$.
\end{defn}

Some properties of asymmetric stable matching problems are well
known.  Since the sets have different cardinalities, some elements
will remain single in any matching.  It is known that, in any stable
matching, all persons of the smaller set are matched.  Regarding the
larger set, it can be partitioned into two sets: in the first all
persons will be matched in all stable matchings; in the second one,
none will be matched~\cite{McVitie70}.
%%


\subsection{The HRT Problem}

The HRT problem is a many-to-one generalization of the SMTI problem. The
previously presented definitions for SMTI can be adapted to
HRT~\cite{Irving2000,Manlove2002,Iwama2008}.

\begin{defn}
  (HRT problem) An HRT instance consists of $n_1$ residents and $n_2$
  hospitals. Each resident $r$ ranks some hospitals in weak order of
  preference and each hospital $h$ ranks its applicants, i.e.  those
  residents who have ranked that hospital, in weak order of preference (note
  that both preference lists can contain ties). Each hospital $h$ also has a
  capacity $c_h \in Z^+$ indicating its number of available posts.
\end{defn}

\begin{defn}
  \label{def:matching2}
  (HRT - Matching) Given an HRT instance, a \emph{matching} $M$ represents a
  (possibly partial) many-to-one matching of residents and hospitals. $M$ is
  a set of resident-hospital pairs $(r,h)$ where $r$ and $h$ are acceptable
  to each other, with each resident appearing in at most one pair of $M$ and
  each hospital $h$ in a number of pairs that is bounded by its capacity
  $c_h$.  If a resident $r$ is not matched in $M$, we say that $r$ is
  \emph{unassigned} in $M$. A hospital $h$ with $a$ assigned residents is
  said to be under-subscribed if $a < c_h$ and full if $a = c_h$. The
  \emph{size} of a matching $M$ is the cardinality of $M$.
\end{defn}

The notion of weak stability we discussed earlier also applies to HRT.

\begin{defn}
  (HRT - Blocking Pair) In a matching $M$, a pair $(r,h)$ is a
  blocking pair iff (a) $r$ and $h$ accept each other and (b)
  $r$ is either unassigned in $M$ or strictly prefers $h$ to his
  assigned hospital, and (c) $h$ is either under-subscribed or
  strictly prefers $r$ to the worst resident assigned to it.
\end{defn}

\begin{defn}
  (HRT - Stable matching) Given an HRT problem instance, a matching
  $M$ is stable iff it has no blocking pairs.
\end{defn}

Stable matchings for a given HRT problem may be of different sizes.
Analogously to MAX-SMTI, the MAX-HRT problem is the optimization
problem consisting in finding a stable matching of maximal size.
%%%%%%% Re-write??
The MAX-HRT problem has also been shown to be
NP-Hard~\cite{Manlove2002}.  Finding maximal matchings is particularly
important for schemes where the matching is large-scale and done
centrally because, once the process concludes, unmatched residents
will endure a disappointing career and personal setback, and typically
reenter a new matching process the following year.
%%%%%%%
% This is not clear.  What does *centralized* matching scheme have to
% do with this?
% ** DONE ** clarify "centralized" as personal setback
Rejected residents may even have to apply to residency programs in
other regions or countries, which would entail negative consequences
for the health care system; see~\cite{Roth1984}.

\subsection{Converting an HRT Problem into an SMTI Problem}
\label{cloning}

From a formal point of view, the HRT and the SMTI problems are very similar.
On one hand an SMTI problem is a special case of HRT in which each hospital
$h$ has a capacity $c_h=1$.  On the other hand it is possible to reduce an
HRT problem to an SMTI problem using the \emph{cloning technique},
see~\cite{Dubins1981,Gusfield1989,Roth1990}.  Basically this comes down to
replace each hospital $h$ with a quota $c_h$ by its $c_h$ copies (each one
being a \emph{position}, i.e.~a single post offered by a hospital).  The
resulting SMTI problem consists of a set of \emph{positions} and a set of
residents.  Each position has the same preference list as its ``root''
hospital.  In the residents' preference lists, each hospital $h$ is replaced
by a sequence composed of the corresponding $c_h$ positions (resulting in a
tie).  Solving this SMTI problem consists in finding a matching $M$ between
residents and positions.  It is trivial to convert $M$ back into a solution
of the original HRT problem (at the cost of a linear increase in the size of
the problem).  With cloning we can convert an HRT problem into an asymmetric
SMTI problem, in polynomial time (see definition~\ref{def:ASMTI}).

% We use the term ``asymmetric'' because
% the resulting SMTI problem can have sets of different sizes.

% \begin{defn}
%   (Asymmetric SMTI problem) An asymmetric SMTI instance is specified by (a)
%   two sets $\M$ and $\W$ of cardinality $m$ and $n$ respectively, (b) a
%   ranking function $R : \M \times \W \to \{1 \ldots n+1\}$ and $R : \W \times
%   \M \to \{1 \ldots m+1\}$.
% \end{defn}
% Note that the only increase of generality over the standard SMTI
% formulation is that $m$
% is not required to be identical to $n$.
% Some properties of asymmetric SMP are known.  Since the sets have
% different cardinalities, some elements will remain single in any
% matching.  It is known that, in any stable matching, all persons of
% the smaller set are matched.  Regarding the larger set, it can be
% partitioned into two sets: in the first all persons will be matched in
% all stable matchings; in the second one, none will be
% matched~\cite{McVitie70}.

% Coming back to the original HRT problem, it is known that if in a stable
% matching a hospital is under-subscribed it will obtain the same set of
% residents in all stable matchings.

When cloning an HRT problem it is useful to characterize the $c_h$
clones appearing in the corresponding SMTI problem since they play the
same role (they are interchangeable).

\begin{defn}
  (Equivalence relation in SMTI) Given an SMTI instance,
  $m_1, m_2 \in \M$ are said to be \emph{equivalent} (written
  $m_1 \sim m_2$) if~~$\forall w, R(m_1,w)=R(m_2,w)$. Similarly,
  $w_1, w_2 \in \W$ are said to be equivalent if~~
  $\forall m, R(w_1,m)=R(w_2,m)$.  
  % Note that $\sim$ is an equivalence relation.
\end{defn}

A given SMTI problem may have equivalent elements or not.  When we
translate an HRT problem into SMTI we get equivalences: the $c_h$
elements corresponding to a hospital $h$ are all pairwise equivalent.

The cloning technique is important from a theoretical point of view since it
makes it possible to ``import'' most of the results established for SMTI
(see~\cite{Gusfield1989,Irving2000,Irving2003}).  We think it is also
important from a practical point of view since it is easy to adapt an SMTI
solver for the HRT case.  It is worth noticing that this approach has not
been widely studied in the literature.  Obviously, cloning increases the size
of the instance by a factor of $\Omega(n)$.  Even if the resulting memory
increase is not a problem with modern computers, it slows down the (variants
of the) GS algorithm by the factor $\Omega(n)$ (the same holds for some
approximation algorithms as well).  In this paper we show that this way of
handling HRT is not a drawback for our local search approach.


\subsection{Solving SMTI and HRT Problems}

Different approaches have been studied in order to solve SMTI and HRT
problems.  Naturally, several studies used the GS algorithm as a
starting point. 
% The GS algorithm is based on a sequence of proposals
% from the men to the women. The resulting matching is \emph{man-optimal}
% (every man has the best possible partner in any stable matching) and
% \emph{woman-pessimal} (each woman has the worst partner she can have in any
% stable matching)~\cite{McVitie71}.  This algorithm is said
% \emph{man-oriented}. Reversing the roles of the men and women (i.e.~swapping
% the matrices} will produce a \emph{woman-oriented} solution.
From a given problem instance, it is possible to
obtain a stable matching by applying a \emph{tie-breaking} algorithm,
which consists in breaking the ties arbitrarily (i.e.~imposing a
strict order in the preference lists) and then applying the GS
algorithm.  However, the way ties are broken severely influences the
size of the resulting matching, which can be far from the optimum.
Indeed it is known that, for both SMTI and HRT, the size of any two
stable matchings can differ by at most a factor 2~\cite{Irving2008}.
Unfortunately, the NP-hardness of the optimization problem leaves
little hope for the existence of an efficient algorithm to optimally
break the ties.  Some specializations of the GS algorithm have been
designed for restricted cases of SMTI, e.g.~when the preference list
of each man has at most 2 women~\cite{DBLP:journals/jda/IrvingMO09}.

At present, only \emph{complete methods} are able to optimally solve a
general SMTI or HRT problem.  These methods consider the whole search space,
either by an exhaustive search or by pruning regions which have no chance to
contain any solution.  \cite{Gent2001} propose a Constraint Programming (CP)
method for the SM problem, which includes the introduction of new global
constraints to achieve efficient consistency~\cite{Unsworth2013,Manlove2007}.
Few papers are devoted to the MAX-SMTI variant.  The reference
paper~\cite{Gent2002} is more a study of the properties of SMTI problems of
limited size (i.e.~$n \leq 60$) than a search for efficient encodings to
solve SMTI using CP.  Surprisingly, SAT solvers have not been extensively
used for SMTI.  The work by~\cite{Gent2002a} proposes a SAT encoding for SMTI
and uses the Chaff solver for its resolution.  Linear
programming~\cite{Roth93,VandeVate89} and integer
programming~\cite{Kwanashie2013} have been also studied for the HRT problem.
Moreover, these optimization problems being NP-hard leaves little or no
hope for the existence of a complete method to optimally solve large instances.
% However efficient complete solvers may be, some problems instances remain
% totally out of reach because of the combinatorial explosion due to the
% exponential search space growth with respect to the size of the problem.

% TODO connect the paragraphs
%To tackle these problems it is necessary to resort to 
On the other hand, \emph{incomplete methods} are a way of dealing with
intractable problems.
% Hard problems are frequently solved using which
These methods are designed to provide good solutions (possibly
sub-optimal) in an acceptable amount of time.  Such is the case of
\emph{approximation algorithms}, i.e.~algorithms running in polynomial
time yet able to guarantee solutions within a constant factor of the
optimum~\cite{Irving2008}.  SMTI has been shown to be an
APX-hard problem~\cite{Halldorsson2003} and most of the recent research
focuses on designing efficient approximation
algorithms~\cite{Kiraly2011}.  An $r$-approximation algorithm always
finds a stable matching $M$ with $|M| \geq |M_{opt}| / r$ where
$M_{opt}$ is a stable matching of maximum size.  The above mentioned
\emph{tie-breaking} algorithm can be seen as a 2-approximation
algorithm.  On the other hand, SMTI cannot be approximated within a
factor 21/19 and probably not within a factor of 4/3
either~\cite{Halldorsson2007}.  Currently, the best known algorithms are
3/2-approximations~\cite{Mcdermid2009,Kiraly2013,Paluch2014}.  These
algorithms produce a single solution for a given problem instance,
even though it is often useful to provide multiple (quasi-)optimal
solutions.  Most of these algorithms can be extended to handle HRT
problems with the same performance guarantee.

Another approach to solve SMTI and HRT problems consists in using
\emph{heuristics}.  In~\cite{Irving2009}, the authors propose two specialized
dedicated heuristic methods to solve both SMTI and HRT for the restricted
case where ties are \textit{only} present in one side.  These methods are
based on the extended version of the GS algorithm~\cite{Gusfield1989}, the
\textit{Hospital-oriented} and \textit{Resident-oriented} algorithms.  These
algorithms use specialized techniques to break the ties, using maximum
cardinality bipartite matching and network flow analysis.  On the other hand,
meta-heuristics are higher-level procedures which can be applied to a wide
variety of problems~\cite{Blum2003}.  Many meta-heuristics are based on
\emph{Local Search} (LS), i.e.~starting from a \emph{configuration} (an
assignment of the variables), they try to improve it iteratively.  For this
they explore a limited \emph{neighborhood} (configurations which are close to
the current one) and select the most promising one (according to some
criterion), hence the term ``local''.  This iterative process stops when a
solution is found or when a given time limit is reached (in the latter case,
the best solution found so far is returned).  Local search solvers make
choices which limit the part of the search space which actually gets visited,
enough so to make problems tractable.  Obviously there is no guarantee that
the optimal solution is ever found.  However, local search algorithms have
been successfully applied to many real-life large-scale problems.  Recently,
a local search meta-heuristic has been successfully applied to SMTI.
In~\cite{Gelain2013}, the authors propose LTIU, a local search algorithm with
very good performance.  In this paper we propose a yet more efficient local
search procedure for SMTI and HRT which, in addition, can be easily
parallelized thereby expanding its performance advantage.


 
% Local Variables:
% mode: tex
% mode: reftex
% TeX-master: "SMTI-HRT"
% End:


