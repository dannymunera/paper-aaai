The stable matching (SM) problem was introduced by Gale and Shapley in
their seminal 1962 paper~\cite{Gale1962}.  An SM problem can be stated as
follows: given a set of $n$ men and a set of $n$ women, each of whom having
ranked all members of the other set in a strict order of preference, find a
\emph{matching} (a one-to-one correspondence between the men and the women)
such that the there is no man-woman pair where both prefer each other than
their current partner. The last criterion is called \emph{stability} and is a
desirable property since it ensures, according to stated preferences, that
there is no man-woman pair both of which have incentive to elope -- such a
pair is called a \textit{blocking pair}.  Gale and Shapley proved that such a
stable matching always exists and proposed an $O(n^2)$ algorithm (called GS
in what follows) to find one.

However, requiring \textit{each} member to rank \textit{all} members of the
opposite sex in a \textit{strict} order is too restrictive for many
real-life, large-scale applications.  A natural variant of SM is the
\emph{Stable Matching with Incomplete List and Ties} (SMTI)
problem~\cite{Iwama1999,Manlove2002}.  In SMTI, the preference lists
may include ties (to express indifference) and may be incomplete (to
express that some partners are unacceptable).  A stable matching
always exists for SMTI and can be easily obtained by arbitrarily
breaking the ties and applying the GS algorithm.  However, with the
introduction of ties and incompleteness in the preference lists, the
stable matchings for an instance of SMTI may have different sizes.  It
is thus desirable to find the stable matching of maximal size (that
is, with the smallest number of singles).  This optimization problem
(called MAX-SMTI) has been shown to be NP-hard, even for very
restricted cases~\cite{Iwama1999,Manlove2002}.  This problem has
attracted a lot of research in recent years since it is at the heart
of a wide variety of important real-life
applications~\cite{Iwama2008,Manlove2002}.  Indeed, matching problems
can be found in several settings, such as car sharing or bipartite
market sharing, job markets and social networks.  Many of these
applications involve very large sets.

Among these applications, one of the most studied ones is the
\textit{Hospitals/Residents problem} (HR)~\footnote{Also known as the
  \textit{College Admissions problem} in the original Gale-Shapley paper.}.
This problem consists of a set of $n_1$ residents who apply to $k$ positions
distributed among $n_2$ hospitals (residents can be viewed as men and hospitals
as women).  The preference list of a resident consists of the ordered list of
\emph{acceptable} hospitals.  The preference list of a hospital contains the
ordered list of residents who apply to it.  We consider an extension where
all preference lists are allowed to contain ties (this problem is called
HRT).  Obviously all lists are incomplete (residents only apply to a subset
of the hospitals and hospitals rank their corresponding candidates).  In
addition, each hospital has a \emph{capacity}, which indicates the maximum
number of positions it offers.  The problem consists in finding a (maximum
size) stable matching between residents and hospitals (thus satisfying the
preference lists) and satisfying the capacities (each resident being assigned
to at most one hospital and the number of residents assigned to any hospital
cannot exceed its capacity).  %From a theoretical point of view, SMTI is a
%special case of HRT~\cite{Irving2000,Irving2009,Manlove2002}.  
The HRT problem has many practical applications, e.g.~assignment of
applicants to positions in job markets.  In the medical domain, there are
national programs in various countries.  The most famous being the National
Resident Matching Program (NRMP) in the USA, the Canadian Resident Matching
Service (CARMS), the Scottish Foundation Allocation Scheme (SFAS) or the
Japan Residency Matching Program (JRMP).  As expected, such programs involve
very large sets.  Unfortunately, as is the case for SMTI, stable matchings
may come in different sizes and the problem of finding a stable matching of
maximum cardinality (called MAX-HRT) is NP-hard (even for restricted cases,
e.g.~if the ties are only allowed on one
side)~\cite{Manlove2002,DBLP:journals/jda/IrvingMO09}.  Finding an efficient
algorithm to solve HRT problems is thus a true challenge with many real
applications.


In this paper we propose an efficient local search procedure for SMTI.  To
this end, we show how to model SMTI problems as permutation problems and
solve them using a local search procedure, based on \emph{Adaptive Search}
(AS)~\cite{Codognet2001,Codognet2003}.  We compare experimentally the
sequential version of our solver with state-of-the-art exact and approximate
algorithms to solve such problems, showing significant improvement in
performance or solution quality.  Moreover, our implementation uses a
parallel framework~\cite{Munera2014a,Munera2014b} written in the X10
programming language~\cite{Charles2005,Saraswat2012}.  We show that the
independent parallel version exhibits a significant speedup when increasing
number of cores and the cooperative version achieves even higher performance
speedup on average and behaves very well on hard instances.

In a second part, we show how this work can be extended to attack HRT
problems.  The key idea of our approach is to only do minimal changes on the
underlying solver.  For this, an HRT problem instance is first converted to
an SMTI-like problem using the well-known \emph{cloning technique}, which
basically replaces a hospital with a capacity $c$ by $c$ copies.  The
resulting SMTI problem is then solved with our cooperative parallel solver.
We experimentally compare our cooperative HRT solver with state of the art
algorithms (an exact method and two dedicated heuristics).  The results show
that our solver can compete favorably with these dedicated methods.
Following this approach we discovered new optimizations to incorporate to the
underlying SMTI solver, tailored for the special case of HRT.  It turns out
that these optimizations will improve performance in solving any SMTI
problem.

The rest of the paper is organized as follows: section~\ref{sec:background}
provides background on SMTI and HRT problems.  Section~\ref{sec:local-search}
presents the AS method: a local search meta-heuristics.
Section~\ref{sec:AS-SMTI} is devoted to SMTI problems.  In this section we
present the AS modeling and evaluate the performance of the AS implementation
both in sequential and parallel.  Finally, Section~\ref{sec:AS-HRT} presents
our extension of the previous algorithm for the HRT case.

% Local Variables:
% mode: latex
% mode: reftex
% TeX-master: "SMTI-HRT"
% End:


