\section{Local Search}
\label{sec:local-search}

Heuristic (i.e.  non-complete) methods have been used in Combinatorial
Optimization for finding optimal or near-optimal solutions since a few
decades, originating with the pioneering work of Lin on the Traveling
Salesman Problem \cite{Lin65}. In the last few years, the interest for the
family of Local Search methods for solving large combinatorial problems has
been revived, and they have attracted much attention from both the Operations
Research and the Artificial Intelligence communities, see for instance the
collected papers in \cite{Aarts97} and \cite{Reeves96}, the textbook
\cite{Fogel2000} for a general introduction.  Although local search
techniques have been associated with basic hill-climbing or greedy
algorithms, this term now encompasses a larger class of more complex methods,
the most well-known instances being simulated annealing, Tabu search,
iterated local search, variable neighborhood search, ant colony optimization
genetic algorithms and extremal optimization, usually referred as
``meta-heuristics''.  They work by iterative improvement over an initial
state and are thus \emph{anytime algorithms} well-suited to reactive
environments. Consider an optimization problem with cost function which makes
it possible to evaluate the quality of a given \emph{configuration}
(assignment of variables to current values) and a transition function that
defines for each configuration a set of "neighbors". The basic algorithm
starts from a random configuration, explore the neighborhood, selects an
adequate neighbor and then moves to the best candidate.  This process will
continue until some satisfactory solution is found. To avoid being trapped in
local optima, adequate mechanisms should be introduced, such as the adaptive
memory of Tabu search, the cooling schedule of simulated annealing or similar
stochastic mechanisms.  Very good results have been achieved by dedicated and
finely tuned local search methods for many problems such as the Traveling
Salesman Problem, scheduling, vehicle routing, cutting stock, etc. Indeed,
such techniques are now the most promising approaches for dealing with very
large search spaces, when the problem is too big to be solved by complete
methods such as constraint solving techniques.

\subsection{Adaptive Search}
Adaptive Search (AS) was proposed in~\cite{Codognet2001} as a
generic, domain-independent, constraint-based local search method.
This meta-heuristic takes advantage of the model of the problem in
terms of constraints and variables in order to guide the search more
precisely than a single global cost function.  AS starts from a random
assignment of the variables (i.e. a \emph{configuration}) and,
iteratively, tries to improve it, modifying one variable at a time
until a solution is found.  To this end AS:

\begin{itemize}
\item Needs to model the constraints with heuristic functions that
  compute an approximate degree of satisfaction of the goals (the
  current \emph{error} on the constraint);
\item Combines these errors to compute the global cost of a
  configuration;
\item For each variable, aggregates the errors of constraints in which
  it occurs, and repairs the \emph{worst} variable (highest error)
  to the most promising value;
\item Maintains a short-term memory (e.g. \emph{tabu list}) of
  recently modified variables which led to local minima, together with
  a reset mechanism (i.e. \emph{iterative local search}).
\end{itemize}

AS has shown good performance on combinatorial problems such as
classical CSPs, or the Costas Array Problem~\cite{Caniou2014}.


\subsection{Parallel Local Search}
\label{sec:parallel-LS}
Parallel versions of local search procedures have been proposed in the
past~\cite{Alba2005,Alba2013,Diaz10CellBE}.  In this article we are
interested in \emph{multi-walks} methods (also called
\emph{multi-starts}) which consist in a concurrent exploration of the
search space, either \emph{independently} or \emph{cooperatively} with
some communication between concurrent processes.  The
\emph{Independent Multi-Walks} method (IW)~\cite{Verhoeven1995} is
easiest to implement since the solver instances do not communicate
among themselves, thereby granting them a natural mechanism of
search-space diversification.  Nevertheless, the parallel performance
gain tends to flatten out when scaling beyond a hundred
processors~\cite{Caniou2014}.  In the \emph{Cooperative Multi-Walks}
(CW) method~\cite{Toulouse2004}, the solver instances communicate to
exchange information, hoping to hasten the search process.  However,
implementing an efficient cooperative method is a very complex task:
several choices have to be made concerning the communication which
interfere with each other and which, moreover, are
problem-dependent~\cite{Toulouse2004}.

%% ** DONE ** diversification is inherent to IW, intensification comes
%% as an extra behavior: the main purpose of cooperative.
%% diversification is further stressed by CW

\subsubsection{Cooperative Parallel Local Search Framework}
We build on the framework for Cooperative Parallel Local Search
({\coop}) proposed in~\cite{Munera2014a,Munera2014b}.  This framework
--- available as an open source library, written in the X10
programming language --- allows the programmer to tune the search
process through an extensive set of parameters.  {\coop} augments the
IW strategy with a tuneable communication mechanism, which allows for
the cooperation between the multiple instances so as to complement the
inherent diversification strategy of IW, by balancing it with
search-space intensification.  The same mechanism can also be used to
strengthen the natural diversification inherited from IW.

\textit{Explorer nodes} are the basic components in the framework:
each consists in a local search solver instance.  The point is to use
all the available processing units by mapping each \textit{explorer
  node} to a physical core.  Explorer nodes are grouped into
\textit{teams}, of size $NPT$. This parameter is directly related to
the trade-off between intensification and diversification. $NPT$ can
take values from $1$ to the maximum number of nodes (frequently linked
to maximum number of available cores in the execution). When $NPT$ is
equal to 1, the framework coincides with the IW strategy, it is
expected that each 1-node team be working on a different region of the
search space, without seek parallel intensification. When $NPT$ is
equal to the maximum number of nodes (creating only 1 team in the
execution), the framework has the maximum level of parallel
intensification, but it is not possible to force parallel
diversification between teams. 

Each team seeks to \emph{intensify} the search in the most promising
neighborhood found by any of its members.  The parameters which guide
the intensification are the \textit{Report Interval} ($R$) and
\textit{Update Interval} ($U$): every $R$ iterations, each explorer
node sends its current configuration and the associated cost to its
\textit{head node}.  The head node is the team member which collects
and processes this information, retaining the best configurations in
an \textit{Elite Pool} ($EP$) whose size $|EP|$ is parametric.  Every
$U$ iterations, explorer nodes randomly retrieve a configuration from
the $EP$, in the head node (see Figure~\ref{fig-CPLS-team}).  An
explorer node may \emph{adopt} the configuration from the $EP$, if it
is ``better'' than its own current configuration with a probability
\textit{pAdopt}.  Simultaneously, the teams implement a mechanism to
cooperatively \emph{diversify} the search, i.e.~they try to extend the
search to different regions of the search space.

Typically, each problem benefits from intensification and
diversification on some level. Therefore, the tuning process of the
CPLS parameters provides an appropriate trade-off between the use of
the parallel mechanisms of intensification and diversification, in the
hope of obtaining a better performance than the non-cooperative
parallel solvers (e.g. Independent Multi-Walks).
%resulting in an  for each kind of problem. 
A detailed description of this framework may be found
in~\cite{Munera2014a}.

\begin{figure}[htb]
  \begin{subfigure}[b]{0.5\textwidth}
    \includegraphics[width=1\columnwidth]{images/TeamStrategy-New}
    \caption{}
    \label{fig-CPLS-global}
  \end{subfigure}%
  \begin{subfigure}[b]{0.5\textwidth}
    \centerline{\includegraphics[width=0.8\columnwidth]{images/TeamStructure}}
    \caption{}
    \label{fig-CPLS-team}
  \end{subfigure}%
 \caption{Cooperative Framework Description - (a) Global Structure (b)
   Team Structure.}
\end{figure}
 
% Local Variables:
% mode: tex
% mode: reftex
% TeX-master: "SMTI-HRT"
% End:
