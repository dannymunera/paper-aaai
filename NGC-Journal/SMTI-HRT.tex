\documentclass[smallextended]{svjour3}       % onecolumn (second format)

\def\Title{Cooperative Parallel Local Search for Hard Stable Matching Problems}

%\usepackage[tight,footnotesize]{subfigure}
\usepackage{multirow}
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{float}
\usepackage[detect-weight=true]{siunitx}
\usepackage{booktabs}
\usepackage{subcaption}
\usepackage[table]{xcolor}
\usepackage{etoolbox}
\usepackage{moresize}



%\captionsetup{compatibility=false}
\usepackage[pdftex]{graphicx}
\usepackage[pdftex,            
	    pdfauthor={Danny Munera, Daniel Diaz, Salvador Abreu, Francesca Rossi, Vijay Saraswat, Philippe Codognet},
            pdftitle={\Title},
            pdfsubject={Stable Matching Algorithms},
            pdfkeywords={Stable matching, Hospital-Resident, Heuristics, Local Search, Parallelism, Cooperation},
            pdfproducer={Latex with hyperref},
            pdfcreator={pdflatex}]{hyperref}

\def\BL{\vspace{\baselineskip}}


\newtheorem{defn}{Definition}

% customize algorithms
\makeatletter
\renewcommand{\ALG@beginalgorithmic}{\small}
\def\BState{\State\hskip-\ALG@thistlm}
\newlength{\trianglerightwidth}
\settowidth{\trianglerightwidth}{$\triangleright$~}
\algnewcommand{\LineComment}[1]{\Statex \hskip\ALG@thistlm $\triangleright$ #1}
\algnewcommand{\LineCommentCont}[1]{\Statex \hskip\ALG@thistlm%
  \parbox[t]{\dimexpr\linewidth-\ALG@thistlm}{\hangindent=\trianglerightwidth \hangafter=1 \strut$\triangleright$ #1\strut}}
\makeatother
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
% end customize algorithms


\newcommand{\coop}{CPLS}
\newcommand{\Seconds}[1]{#1\nobreak\hspace{.05em}\textit{s}}
\newcommand{\Minutes}[1]{#1\nobreak\hspace{.05em}\textit{m}}

\definecolor{palegreen}{rgb}{0.6, 0.98, 0.6}
\definecolor{camel}{rgb}{0.76, 0.6, 0.42}
\definecolor{atomictangerine}{rgb}{1.0, 0.6, 0.4}
\definecolor{coral}{rgb}{1.0, 0.5, 0.31}
\definecolor{deepsaffron}{rgb}{1.0, 0.6, 0.2}
\definecolor{orangewebcolor}{rgb}{1.0, 0.65, 0.0}
\definecolor{pastelorange}{rgb}{1.0, 0.7, 0.28}

\newcolumntype{N}[1]{S[table-format=#1]}
\setlength{\tabcolsep}{4pt}

\robustify\bfseries
\newcommand{\B}[1]{\bfseries #1}

\newcommand{\G}[1]{{\cellcolor[gray]{0.8}} #1}
\newcommand{\R}[1]{{\cellcolor[gray]{0.8}} #1}

\newcommand{\M}{\mathcal{M}}
\newcommand{\W}{\mathcal{W}}

\begin{document}

%\markboth{Danny Munera et al.}{\Title}

\title{\Title}

\author{
Danny Munera \and Daniel Diaz \and Salvador Abreu \and 
Francesca Rossi \and Vijay Saraswat \and Philippe Codognet}

\institute{D. Munera \at
  University of Antioquia\\
  \email{danny.munera@udea.edu.co}          %  \\
  % \emph{Present address:} of F. Author  %  if needed
  \and
  D. Diaz \at
  University of Paris 1/CRI\\
  \email{daniel.diaz@univ-paris1.fr} 
  \and
  S. Abreu \at
  University of \'Evora/LISP/CRI\\
  \email{spa@di.uevora.pt}
  \and
  F. Rossi \at
  University of Padova\\
  \email{frossi@math.unipd.it}
  \and
  V. Saraswat \at
  IBM TJ Watson Research Center\\
  \email{vsaraswa@us.ibm.com}
  \and
  P. Codognet \at
  JFLI-CNRS/UPMC University of Tokyo\\
  \email{codognet@is.s.u-tokyo.ac.jp}
}

\date{Received: date / Accepted: date}
% The correct dates will be entered by the editor
\maketitle

\begin{abstract}
  Stable matching problems have several practical applications.  If
  preference lists are truncated and contain ties, finding a stable
  matching with maximal size becomes computationally difficult.  One
  important variant is the hospitals/residents problem which have
  several real-life, large-scale applications.

  In this article, we address these problems using a local search
  procedure, based on Adaptive Search and present experimental evidence
  that this approach is considerably more efficient than existing
  state-of-the-art exact and approximate methods.  Moreover, we show that
  performance improves considerably under a parallel framework we
  developed, which includes an embarassingly parallel version but also one
  with collaborative communication.  In fact, the performance gain is such
  that solving very large and hard instances becomes practical.
\end{abstract}


\keywords{Stable matching, Hospital-Resident, Heuristics, Local
  Search, Parallelism, Cooperation}

% \newcommand\blfootnote[1]{%
%   \begingroup
%   \renewcommand\thefootnote{}\footnote{#1}%
%   \addtocounter{footnote}{-1}%
%   \endgroup
% }

% \begin{NoHyper}
% \blfootnote{%
%   \textbf{Note to the reviewers}: %
%   this journal paper presents our results on the SMTI and HRT problems.
%   Regarding the SMTI problem, this article is an extended version of our
%   paper published in the AAAI'2015 conference proceedings~\cite{Munera2015}
%   with additional material we could not put in the conference paper due to
%   space limitations. Several table have been added to
%   Section~\ref{sec:AS-SMTI} devoted to
%   SMTI. Section~\ref{sec:runt-behav-analys} presents a runtime behavior
%   analysis of our sequential solver. Section~\ref{sec:comp-with-local}, which
%   compares our solver with another Local Search method, now includes a
%   discussion explaining why our solver is much faster.
%   Section~\ref{sec:parallel-eval} includes a new section detailing, through
%   several figures, the parameters tuning used in the cooperative parallel
%   algorithm.  Regarding the HRT problems, only preliminary results (in
%   sequential with a limited test set) were presented at the MATCHUP'15
%   Workshop~\cite{Munera2015a}. This journal paper presents our most complete
%   results to date (improved algorithm, cooperative parallel version, very
%   large test set) together with a comparison with 2 state-of-the-art complete
%   and incomplete methods.}
% \end{NoHyper}

  % \textbf{Note to the reviewers}: %
  % this journal paper presents our results on the SMTI and HRT problems.
  % Regarding the SMTI problem, this article is an extended version of our
  % paper published in the AAAI'2015 conference proceedings~\cite{Munera2015}
  % with additional material we could not put in the conference paper due to
  % space limitations.
  % Regarding the HRT problems, only preliminary results (in
  % sequential with a limited test set) were presented at the MATCHUP'15
  % Workshop~\cite{Munera2015a}. This journal paper presents our most complete
  % results to date (improved algorithm, cooperative parallel version, very
  % large test set) together with a comparison with 2 state-of-the-art complete
  % and incomplete methods.}

%\vspace{1em}
\input{1-intro}

\input{2-background}

\input{3-local-search}

\input{4-AS-SMTI}

\input{5-AS-HRT}

\input{6-conclusion}

% \section*{Acknowledgments}
% %\begin{acks}
% The authors are indebted to the team of the University of Glasgow, UK
% -- Robert Irving, David Manlove and Augustine Kwanashie -- for
% providing us with many HRT instance, an HRT instance generator and the
% tie-breaking algorithm.  The experimentation was carried out in the
% \texttt{khromeleque} cluster of the School of Science and Technology
% of the University of \'Evora, partly funded by grants
% ALENT-07-0262-FEDER-001872 and ALENT-07-0262-FEDER-001876 under the
% \emph{Programa Operacional do Alentejo} (INALENTEJO.)
% %\end{acks}

%\section*{References}

% BibTeX users please use one of
%\bibliographystyle{spbasic}      % basic style, author-year citations
\bibliographystyle{spmpsci}      % mathematics and physical sciences
%\bibliographystyle{spphys}       % APS-like style for physics
%\bibliography{}   % name your BibTeX data base
%\bibliographystyle{ACM-Reference-Format-Journals}
\bibliography{library}

\end{document}

% Local Variables:
% mode: latex
% mode: reftex
% End:
