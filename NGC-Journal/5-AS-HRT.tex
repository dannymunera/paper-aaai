\section{The AS-HRT Algorithm}
\label{sec:AS-HRT}



In this section, we propose an algorithm to solve the HRT problem based on
the algorithm presented above (we call this extension
AS-HRT)%~\cite{Munera2015a}
. Our goal is to obtain an HRT solver with \textit{minimum changes} from our
AS-SMTI algorithm.  For this purpose, AS-HRT resorts to the cloning technique
described in Section~\ref{cloning}.  To implement the extension to HRT
problems, we thus developed a pre-processor and a post-processor.  The
pre-processor converts an HRT problem instance $I$ into an equivalent SMTI
problem instance $I'$, recording a corresponding mapping information.  The
post-processor system takes the matching found by the AS-SMTI solver for $I'$
and converts it back (using the mapping information created by the
pre-processor) to provide a solution for the initial HRT problem. 
%This process is depicted in Figure~\ref{fig:hrt-approach}.


% \begin{figure}[b]
%   \centerline{\includegraphics[scale=0.5]{images/HRT-approach.pdf}}
%   \caption{Description of the solver extension to HRT problems} 
%  \label{fig:hrt-approach}
% \end{figure}


As seen in Section~\ref{cloning}, cloning an HRT problems creates $c_j$
copies for the hospital $h_j$ which all play the same role and are
interchangeable (they are \emph{equivalent} noted with the symbol
$\sim$). When walking through several matchings, it is desirable to avoid
exploring equivalent matchings (i.e. those whose difference only concerns
equivalent elements).  Indeed, only one matching from an equivalence class
needs to be examined.  It is worth observing that given a matching $M$ if
$(m,w)$ and $(m',w')\in M$ give rise to a blocking pair $(m,w')$, then $m
\not \sim m'$ and $w \not \sim w'$.  Therefore, the swap executed by the main
loop of the AS-SMTI algorithm to fix a BP already ensures that a matching is
not changed to an equivalent matching.  However, it is not the case of the
reset procedure invoked to escape a local minimum which performs random
swaps.  This can be seen as a form of symmetry breaking.
% TODO: Should we do this for the journal version?
We have not yet improved this point (if a reset procedure swaps two
equivalent elements the local minimum is not escaped and another reset must
occur resulting in a waste of time).  To be fully aware of equivalences in
the current AS-SMTI algorithm, we could in addition first compute the
\emph{equivalence classes} for $R$, i.e.~the sets of equivalences: this is a
linear operation, and later prohibit swaps between equivalent elements
in the reset procedure. 
%We plan to do this in a future implementation of the algorithm.

We only had to introduced minor changes to the AS-SMTI solver for the
HRT case: we first adapted our algorithm to manage asymmetric SMTI
problems since, a cloned HRT problem gives rise to an asymmetric SMTI
problem with $n_1$ men and $n_2$ women.  As our Adaptive Search solver
is specialized for permutation problems, we simply use a vector of
$max(n_1,n_2)$ values and creates some ``dummy'' elements for the
smaller set.  We also optimized the behavior of the AS-SMTI reset
function: in the original reset function (see
algorithm~\ref{algo:reset}), if there are singles in the current
matching, the algorithm selects a random single $m$ and performs a
swap with another random man $m'$ (forcing the single $m$ to be
matched to any other woman).  This mechanism is efficient for SMTI
problems, however, HRT problems typically have short preference lists:
this mechanism will thus frequently pair $m$ with an unacceptable
woman.  For instance, if the length of the preference list is 5 and
the size of the problem is 1000, the probability to match $m$ with an
acceptable woman is only 0.005.  We modified this part of the reset
function as follows: if there are singles in the match, the algorithm
selects a random single $m$ and selects a random woman $w$ \emph{from
  his preference list}.  The algorithm then searches for the man $m'$
currently matched to $w$ to perform the swap ($X_m \leftrightarrow
X_{m'}$).  This ensures that the new partner of $m$ is always
acceptable.  It is noteworthy that this optimization now also benefits
other SMTI problems, in general.

\subsection{AS-HRT Evaluation}
In this section we compare our AS-HRT algorithm against other
state-of-the-art methods to solve HRT problems.  We selected
representatives of two different approaches: an Integer Programming
(IP) solver developed by ~\cite{Kwanashie2013} and two specialized
heuristics described in ~\cite{Irving2009}.

\subsubsection{Comparison with an Integer Programming Solution}
\label{sec:compar-IP}

We first compare our AS-HRT against the Integer Programming (IP) model proposed
by~\cite{Kwanashie2013}. For the experimentation, the authors used a random
generator whose parameters are: number of residents $n_1$ (size of the
problem), number of hospitals $n_2$, number of posts $C$ (the posts are
randomly distributed among hospitals), length of the residents' preference list
$l$, probability $td$ that an element is tied to the next element in a
preference list (the \emph{tie density} $td$ is similar to the $p2$ parameter
of the generator used for SMTI).  With $td=0$ the problem has no ties (pure HR
instance). With $td=1$ each preference list consists in exactly 1 tie.

% \begin{itemize}
% \item Number of residents $n_1$ (size of the problem).

% \item Number of hospitals $n_2$.

% \item Number of posts $C$ (the posts are randomly distributed among
%   hospitals).

% \item Length of the residents' preference list $l$.

% \item Probability $td$ that an element is tied to the next element in a
%   preference list. The \emph{tie density} $td$ is similar to the $p2$
%   parameter of the generator used for SMTI in
%   Section~\ref{sec:SMTI-eval-problems}.  With $td=0$ the problem has no ties
%   (pure HR instance). With $td=1$ each preference list consists in exactly 1
%   tie.
% \end{itemize}

The authors kindly provided us with their complete test set (including execution
times and optimal solutions when known).  We selected the instances generated
with the following parameters: $n_1=300$, $n_2=\lfloor n_1 \times 0.07
\rfloor = 21$, $C=300$, $l=5$ and $td$ ranging over $[0,1]$ with step $0.05$
(ties only appear in the hospitals’ preference lists).  This data set consists
of 10\,000 instances for each $td$ value.  The IP formulation turned out to be
very effective: most of these instances were (optimally) solved by the CPLEX
IP solver within a timeout of \Seconds{300}.  Only 4 instances (with $td \in
[0.75,0.9]$) could not be optimally solved.  CPLEX was run on a Linux machine
with 8 Intel Xeon clocked at 2.5 GHz (a bit faster than our machine).

As to our solver, we used the parallel version of the AS-HRT algorithm
running on 64 cores~\footnote{Both the source code and problem
  instances are available at
  \texttt{http://cri-hpc1.univ-paris1.fr/hrt/}}.
%\footnote{The  source code is available at
%  \texttt{https://github.com/dannymrock/CSP-X10/releases/tag/v0.2}}. 
To configure the CPLS framework, we performed a parameter tuning
process similar to that used for SMTI problems.
% (see section~\ref{sec:SMTI-parallel-param}). 
We selected 8 teams, each one
composed of 8 explorers.  Inside a team, each explorer periodically
exchanges information with the corresponding Elite Pool according to
the report interval ($R$) and update interval ($U$).  The best
settings were found to be $R = 200$ iterations, $U = 400$ iterations,
the Elite Pool size $|EP| = 4$ and the probability to adopt the
incoming configuration $pAdopt = 1$ (i.e.~it's determinate).

We configured the AS-HRT solver to stop as soon as the optimal solution is
reached within a timeout of \Seconds{5} (we used a short timeout 
%and limited the number of cores to 64 
in order to be able to run all 10\,000 instances 50 times -- results
are averaged).

In figure~\ref{fig:AS-HRT-IP-qual} we compare the quality of solutions found by
both solvers measuring the size of the returned matching.  Even with a very
short timeout, AS-HRT is able to reach the optimal solutions for all problems
with $td \in [0,0.55)$ and for $td=1$.  For $td \in [0.55,0.95]$, AS-HRT is
sometimes sub-optimal but the returned solution generally only contains 1
more single than the matching found by IP+CPLEX.  On the other hand, AS-HRT is
able to provide better solutions than IP for the 4 cases not optimally solved
by CPLEX (this cannot be viewed in the table since for each $td$ the
displayed result is the average of the 10\,000 instances).

\begin{figure}[htb]
  \centering
  \begin{subfigure}{0.4\textwidth}
    \begin{center}
    %\footnotesize
    %\scriptsize
    \tiny
    \begin{tabular}{cN{3.2}N{3.2}}
     \toprule
     $td$  & {AS-HRT} & {IP} \\
     \midrule
      0    & 297.42 &    297.42  \\
      5    & 297.45 &    297.45  \\
      10   & 297.49 &    297.49  \\
      15   & 297.52 &    297.52  \\
      20   & 297.56 &    297.56  \\
      25   & 297.68 &    297.68  \\
      30   & 297.70 &    297.70  \\
      35   & 297.81 &    297.81  \\
      40   & 297.90 &    297.90  \\
      45   & 298.01 &    298.01  \\
      50   & 298.14 &    298.14  \\
      55   & 298.29 & \G{298.30} \\
      60   & 298.45 & \G{298.46} \\
      65   & 298.62 & \G{298.64} \\
      70   & 298.82 & \G{298.86} \\
      75   & 299.06 & \G{299.13} \\
      80   & 299.33 & \G{299.42} \\
      85   & 299.61 & \G{299.72} \\
      90   & 299.85 & \G{299.93} \\
      95   & 299.99 & \G{300.00} \\
      100  & 300.00 &    300.00  \\
     \bottomrule
    \end{tabular}
    \end{center}
  \end{subfigure}%
  \begin{subfigure}{0.6\textwidth}
    % \RaggedLeft
    \centerline{\includegraphics[scale=1]{images/J-ASvsIP-match-size}}
  \end{subfigure}%

 \caption{AS-HRT vs IP: quality of solutions ($n=300$).}
 \label{fig:AS-HRT-IP-qual}
\end{figure}

  
In figure~\ref{fig:AS-HRT-IP-qual}
%\marginpar{\fbox{Use log Y-scale!}}
we present the average execution time of both the parallel AS-HRT
algorithm and IP method.  It is clear from the figure, that the most
difficult problems are those with a tie density between $0.6$ and
$0.95$.  For instance, when $td=0.85$ (corresponding to the hardest
zone), the parallel AS-HRT takes in average \Seconds{0.78}, while IP
needs \Seconds{18.3}.

\begin{figure}[htb]
  \centering
  \begin{subfigure}{0.4\textwidth}
    \begin{center}
    %\footnotesize
    %\scriptsize
    \tiny
    \begin{tabular}{cN{1.3}N{1.3}}
     \toprule
     $td$  & {AS-HRT}  & {IP} \\
     \midrule
      0    &    0.040  & \G{0.005} \\
      5    &    0.040  & \G{0.006} \\
      10   &    0.040  & \G{0.007} \\
      15   &    0.040  & \G{0.008} \\
      20   &    0.039  & \G{0.010} \\
      25   &    0.041  & \G{0.013} \\
      30   &    0.042  & \G{0.016} \\
      35   &    0.041  & \G{0.022} \\
      40   &    0.047  & \G{0.030} \\
      45   &    0.049  & \G{0.046} \\
      50   & \G{0.060} &    0.071  \\
      55   & \G{0.082} &    0.126  \\
      60   & \G{0.105} &    0.231  \\
      65   & \G{0.176} &    0.516  \\
      70   & \G{0.299} &    1.488  \\
      75   & \G{0.476} &    4.405  \\
      80   & \G{0.653} &   10.715  \\
      85   & \G{0.779} &   18.307  \\
      90   & \G{0.503} &   12.496  \\
      95   & \G{0.073} &    3.292  \\
      100  & \G{0.016} &    0.500  \\
     \bottomrule
    \end{tabular}
    \end{center}
  \end{subfigure}%
  \begin{subfigure}{0.6\textwidth}
    % \RaggedLeft
    \centerline{\includegraphics[scale=1]{images/J-ASvsIP-time-log}}
  \end{subfigure}%

 \caption{AS-HRT vs IP: execution time ($n=300$).}
 \label{fig:AS-HRT-IP-time}
\end{figure}



% The results show that when using $td=1$, the execution time to get
% the optimal solution is \textit{almost} constant, at about $40ms$.
% When the tie density decreases, the average execution time tends to
% be the same as the chosen timeout value.  This behaviour can be
% explained because, when the optimal solution of a problem instance
% is not perfect ($|M| < 300$), the AS-HRT method, which doesn't know
% this, will keep trying to improve on it until the timeout is
% reached.

\subsubsection{Comparison with two Specialized Heuristics}

We now compare AS-HRT with two specialized heuristics described
in~\cite{Irving2009}.  One is based on the hospital-oriented version of the
Gale-Shapley algorithm (``hospital-offer'', called the \textbf{H} algorithm)
and the other on the resident-oriented version (``resident-offer'', called
the \textbf{R} algorithm).  These algorithms implement complex techniques to
break one-side ties, usually in the Hospital's preference lists.  The paper
contains an empirical study on both real data and generated instances.  For
the latter, the authors developped a flexible problem generator whose
parameters control:

\begin{itemize}
\item The number of residents, hospitals and posts offered.
\item The maximum length of the residents' preference list
  % centralised matching schemes usually allow each resident to create a
  % preference list composed of no more than five hospitals, organised in
  % \textit{strict} order of preference.
\item How posts are distributed among hospitals: they can be either
  \emph{uniformly} or \emph{randomly distributed}.
\item The hospitals popularity scheme: either \emph{uniform} or \emph{skewed}
  (the most popular hospitals receive about 5 times more applicants than the least
  popular).
\item How ties are created on hospitals' preference lists (this generator does
  not allow for ties on residents' list).  Two strategies are available: using
  \emph{individual} hospitals' preference list or using a ``master'' preference
  list of residents.  In the first case, ties are created using a tie density
  $td$ as in Section~\ref{sec:compar-IP}.  In the second case, a random
  \emph{score} is assigned to each resident.  A master list of residents is
  created using these scores (residents with a same score will result in a
  tie).  The degree of ties is thus controlled by tweaking the number of
  available scores.
\end{itemize}

The paper reports on the quality of solutions found by the H and R heuristics
for various classes of generated problems.  It also details the results of a
tie-breaking algorithm (called the \textbf{I} algorithm).  This process first
breaks all ties randomly and independently and then solves the resulting
problem with the Gale-Shapley (GS) algorithm.  For a fair comparison, all three
algorithms are allowed to iterate as many times as possible until a timeout of
\Seconds{60} is reached (the best matching found so far is then returned).  All
experiments were conduced on a PC clocked at 2.6 GHz (a bit faster than our
test machine).

Unfortunately, the authors could not provide us neither the instances (neither
real data nor generated instances) nor the H and R algorithms.  Fortunately
they kindly provided us their problem generator and the I algorithm.

We thus used the generator to create instances similar to those of the
reference paper: $1000$ residents, $100$ hospitals, residents' preference list
of length $5$, $1000$ posts, two levels of tie density (medium $td=0.5$ and
high $td=0.9$), with posts both randomly or uniformly distributed among the
hospitals, allowing hospitals to be uniformly popular or skewed, using
individual hospitals' preference list and a master list of residents (with $5$
and $50$ scores).  As in the reference paper, for each parameter combination,
we generated 10 problems instances.

In what follows, we compare the quality of solutions returned by the AS-HRT
algorithm to the H and R heuristics.  We include the result of the I algorithm
both from the reference paper and from running on our test set, to validate the
similarity of both test sets.  The AS-HRT solver was run on 64 cores with two
different timeouts: \Seconds{5} and \Seconds{60}.  For each setting, all 10
problems were solved once and the resulting matching sizes are averaged.

\paragraph{Individual Hospital Preference Lists}

Tables~\ref{tab:HRT-mtd} and~\ref{tab:HRT-htd} display the results obtained for
problems using individual hospital's preference lists with a medium and high
tie density varying the post distribution (random or uniform) and the hospital
popularity (uniform or skewed).  Clearly, both data sets present the same
characteristics since the results of algorithm I on our test set are very close
to those reported in~\cite{Irving2009}.  Globally, algorithm R is better than
algorithm H.  Even using a short timeout of \Seconds{5}, the average matching
size found by AS-HRT is very close to the best obtained with the heuristics~H
and R, and better than algorithm I.  Note that the quality of the solution
increases with the larger timeout of \Seconds{60}.  In the context of a uniform
hospital popularity with a high tie density ($td = 0.9$) AS-HRT is better than
H and R.  In particular, when the post distribution is uniform our algorithm is
able to discover a perfect solutions for all 10 instances, whereas none of the
other algorithms could.


\begin{table}[htb]
  \footnotesize
  \begin{NoHyper}
  \begin{center}
  \begin{tabular}{cccccc}
    \toprule
    \multirow{2}{*}{Source}    &\multirow{2}{*}{Algorithm}&\multicolumn{2}{c}{Random post distribution}  & \multicolumn{2}{c}{Uniform post distribution }\\
    \cmidrule(r){3-4}\cmidrule(l){5-6}
                                & & Uniform h. pop.& Skewed h. pop. & Uniform h. pop. & Skewed h. pop. \\
    \midrule
    \multirow{3}{*}{\begin{tabular}{c}\cite{Irving2009}\\Table V\end{tabular}} 
                                  & H                  & \G{990.0} &    961.5  &    996.3  &    975.0  \\
                                  & R                  &    989.8  & \G{961.8} & \G{996.6} & \G{975.2} \\
                                  & I                  &    989.2  &    960.2  &    996.0  &    974.3  \\
    \midrule
    \multirow{3}{*}{Present work} & I                  &    989.2  &    960.2  &    996.0  &    974.3  \\
                                  & AS-HRT \Seconds{5} &    989.3  &    961.0  &    995.5  &    974.8  \\
                                  & AS-HRT \Minutes{1} &    989.9  &    961.3  &    996.0  & \G{975.2} \\
    \bottomrule
  \end{tabular}
  \end{center}
  \end{NoHyper}
  \caption{Matching size (individual hospital's preference lists, medium tie density $td=0.5$, $n=1000$)}
  \label{tab:HRT-mtd}
\end{table}



\begin{table}[htb]
  \footnotesize
  \begin{NoHyper}
  \begin{center}
  \begin{tabular}{cccccc}
    \toprule
    \multirow{2}{*}{Source}    &\multirow{2}{*}{Algorithm}&\multicolumn{2}{c}{Random post distribution}  & \multicolumn{2}{c}{Uniform post distribution }\\
    \cmidrule(r){3-4}\cmidrule(l){5-6}
                                & & Uniform h. pop.& Skewed h. pop. & Uniform h. pop. & Skewed h. pop. \\
    \midrule
    \multirow{3}{*}{\begin{tabular}{c}\cite{Irving2009}\\Table VI\end{tabular}} 
                                  & H                  &    998.2  &    986.5  &    999.6  &    990.0  \\         
                                  & R                  &    998.3  & \G{988.3} &    999.9  & \G{993.2} \\  
                                  & I                  &    992.3  &    972.1  &    998.2  &    979.4  \\         
    \midrule                                                                                      
    \multirow{3}{*}{Present work} & I                  &    992.3  &    972.2  &    998.2  &    979.5  \\         
                                  & AS-HRT \Seconds{5} &    998.5  &    985.2  &    999.6  &    990.1  \\         
                                  & AS-HRT \Minutes{1} & \G{998.9} &    986.4  &\G{1000.0} &    992.0  \\
    \bottomrule
  \end{tabular}
  \end{center}
  \end{NoHyper}
  \caption{Matching size (individual hospital's preference lists, high tie density $td=0.9$, $n=1000$)}
  \label{tab:HRT-htd}
\end{table}


\paragraph{Master List of Residents}

Tables~\ref{tab:HRT-hs} and \ref{tab:HRT-ls} show the results when using a
master list of residents with a high and a low number of scores.  Again, the
matchings found by Algorithm indicates that our test set is very similar to
that of the reference paper.  When the number of scores is high, AS-HRT and
algorithm H produce matchings of the same quality, both better than algorithm
R.
% number of scores in the master list of residents. ??
The maximum matching size is similar far all the cases we considered and for
all the algorithms.  In contrast, with a low number of scores algorithm R is
better than its competitors.  Even using a short timeout, AS-HRT is always
better than algorithm H.  Again, the quality of solutions returned by AS-HRT
increases using a longer timeout.

AS-HRT is significantly better than algorithm~I for almost all the cases, and
never worse.  Even using a short timeout, AS-HRT performs better than
algorithm~H in most cases (particularly in problems with high tie density or
medium number of scores).  Using a large timeout, AS-HRT obtains a larger
matching size and its results are comparable to algorithm~R, which is the one
that obtains the larger matching sizes.


\begin{table}[htb]
  \footnotesize
  \begin{NoHyper}
  \begin{center}
  \begin{tabular}{cccccc}
    \toprule
    \multirow{2}{*}{Source}    &\multirow{2}{*}{Algorithm}&\multicolumn{2}{c}{Random post distribution}  & \multicolumn{2}{c}{Uniform post distribution }\\
    \cmidrule(r){3-4}\cmidrule(l){5-6}
                                & & Uniform h. pop.& Skewed h. pop. & Uniform h. pop. & Skewed h. pop. \\
    \midrule
    \multirow{3}{*}{\begin{tabular}{c}\cite{Irving2009}\\Table VII\end{tabular}} 
                                  & H                  & \G{966.7} & \G{931.1} & \G{977.5} & \G{944.4} \\
                                  & R                  &    966.3  &    930.4  &    976.9  &    944.0  \\      
                                  & I                  &    966.6  &    931.0  &    977.4  &    944.3  \\      
    \midrule                                                                                   
    \multirow{3}{*}{Present work} & I                  & \G{966.7} & \G{931.1} &    977.4  &    944.3  \\
                                  & AS-HRT \Seconds{5} &    966.6  & \G{931.1} &    977.2  & \G{944.4} \\
                                  & AS-HRT \Minutes{1} & \G{966.7} & \G{931.1} &    977.4  & \G{944.4} \\
    \bottomrule
  \end{tabular}
  \end{center}
  \end{NoHyper}
  \caption{Matching size (with master list of residents, high number of scores (50), $n=1000$)}
  \label{tab:HRT-hs}
\end{table}



\begin{table}[htb]
  \footnotesize
  \begin{NoHyper}
  \begin{center}
  \begin{tabular}{cccccc}
    \toprule
    \multirow{2}{*}{Source}    &\multirow{2}{*}{Algorithm}&\multicolumn{2}{c}{Random post distribution}  & \multicolumn{2}{c}{Uniform post distribution }\\
    \cmidrule(r){3-4}\cmidrule(l){5-6}
                                & & Uniform h. pop.& Skewed h. pop. & Uniform h. pop. & Skewed h. pop. \\
    \midrule
    \multirow{3}{*}{\begin{tabular}{c}\cite{Irving2009}\\Table VIII\end{tabular}} 
                                  & H                  &    987.0  &    958.2  &    997.1  &    966.8  \\
                                  & R                  & \G{993.5} & \G{966.5} & \G{998.9} & \G{975.8} \\
                                  & I                  &    973.0  &    945.3  &    985.5  &    955.0  \\
    \midrule                                                                                                                              
    \multirow{3}{*}{Present work} & I                  &    973.1  &    945.4  &    985.5  &    955.4  \\
                                  & AS-HRT \Seconds{5} &    989.3  &    961.0  &    998.2  &    971.1  \\
                                  & AS-HRT \Minutes{1} &    991.0  &    963.6  &    998.4  &    974.2  \\
    \bottomrule
  \end{tabular}
  \end{center}
  \end{NoHyper}
  \caption{Matching size (with master list of residents, low number of scores (5), $n=1000$)}
  \label{tab:HRT-ls}
\end{table}

These results clearly validate our approach: a simple HRT solver (based on a
simple conversion of the HRT problem to an SMTI instance which is solved by our
cooperative AS-SMTI algorithm) compares very well to highly specialized
heuristics.  In addition, it is possible to improve the quality of solutions by
just supplying more computing resources (number of cores) and/or increasing the
timeout.  No coding changes are thus necessary in order to expand the universe
of practically solvable problems, largely as a consequence of the parallel
scalability of AS-HRT implemented on top of CPLS: all we need is more cores.

% Local Variables:
% mode: tex
% mode: reftex
% fill-column: 79
% TeX-master: "SMTI-HRT"
% End:






